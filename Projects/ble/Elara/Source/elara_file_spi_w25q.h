#ifndef ELARA_FILE_SPI_W25Q_H
#define ELARA_FILE_SPI_W25Q_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * FUNCTIONS
 */
extern int elara_flash_read_init_spi_w25q(void);
extern void elara_flash_write_init_spi_w25q(void);
extern void elara_flash_read_spi_w25q( uint16 len, uint8 *pBuf );
extern uint32 elara_flash_if_flash_enough_spi_w25q(uint32 binSize);
extern int elara_flash_write_spi_w25q(uint16 len, uint8 *pBuf);
extern int elara_flash_write_done_spi_w25q(void);
extern int elara_flash_erase_spi_w25q(void);
extern void elara_flash_lost_connected_spi_w25q(void);

extern uint32 elara_flash_any_thing_else_spi_w25q(void);
extern bool elara_flash_if_erase_finished_spi_w25q(void);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
