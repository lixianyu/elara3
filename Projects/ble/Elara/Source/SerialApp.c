#include <stdio.h>
#include <string.h>
#include "bcomdef.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"

#include "OnBoard.h"
#include "hal_uart.h"
#include "hal_lcd.h"
#include "keyfobdemo.h"
#include "md_uart_profile.h"
//#define DEBUG_SERIAL
#include "SerialApp.h"
#include "elara_led.h"

extern bool gChuanKouTouChuanJustBegin;
extern bool gIsUpdatingAVR;
extern uint8 str_cmp(uint8 *p1, uint8 *p2, uint8 len);
extern bool parse_at_command(uint8 *pBuffer, uint16 length);
extern bool Elara_IfConnected(void);
static void simpleBLE_NpiSerialCallback( uint8 port, uint8 events );
static uint8 sendMsgTo_TaskID;
/* Ative delay: 125 cycles ~1 msec */
#define KFD_HAL_DELAY(n) st( { volatile uint32 i; for (i=0; i<(n); i++) { }; } )

bool simpleBLEChar6DoWrite2 = TRUE;

/*
串口设备初始化，
必须在使用串口打印之前调用该函数进行uart初始化
*/
void SerialApp_Init( uint8 taskID , uint8 mode, uint8 baudRate)
{
    HalUARTInit();
    //调用uart初始化代码
    serialAppInitTransport(mode, baudRate);
    //记录任务函数的taskID，备用
    sendMsgTo_TaskID = taskID;
}

void SerialApp_Init_2( uint8 taskID , uint8 mode, uint8 baudRate)
{
    //调用uart初始化代码
    serialAppInitTransport(mode, baudRate);
    //记录任务函数的taskID，备用
    sendMsgTo_TaskID = taskID;
}

#ifdef ELARA_UNINIT_UART
#define HAL_UART_P0_SEL            0x0C         // Peripheral I/O Select for Rx/Tx.
void SerialApp_Uninit(void)
{
    PERCFG = 0x00;
    P0SEL &= ~(HAL_UART_P0_SEL); // 0: General-purpose I/O, 1: Peripheral function
    P0DIR &= ~(HAL_UART_P0_SEL); // 0: Input,  1: Output
    //P2INP &= ~(0x20);
    P0INP |= (HAL_UART_P0_SEL);
}
#endif

/*
uart初始化代码，配置串口的波特率、流控制等
*/
void serialAppInitTransport(uint8 mode, uint8 baudRate)
{
    halUARTCfg_t uartConfig;

    // configure UART
    uartConfig.configured           = TRUE;
    uartConfig.baudRate             = baudRate;//波特率
    uartConfig.flowControl          = SBP_UART_FC;//流控制
    uartConfig.flowControlThreshold = SBP_UART_FC_THRESHOLD;//流控制阈值，当开启flowControl时，该设置有效
    uartConfig.rx.maxBufSize        = SBP_UART_RX_BUF_SIZE;//uart接收缓冲区大小
    uartConfig.tx.maxBufSize        = SBP_UART_TX_BUF_SIZE;//uart发送缓冲区大小
    uartConfig.idleTimeout          = SBP_UART_IDLE_TIMEOUT;
    uartConfig.intEnable            = SBP_UART_INT_ENABLE;//是否开启中断
    //uartConfig.callBackFunc         = sbpSerialAppCallback;//uart接收回调函数，在该函数中读取可用uart数据
    if (mode == 0)
    {
        uartConfig.callBackFunc         = NULL;
    }
    else//transport mode.
    {
        //uartConfig.callBackFunc         = sbpSerialAppCallbackTrans;
        uartConfig.callBackFunc         = simpleBLE_NpiSerialCallback;
    }

    // start UART
    // Note: Assumes no issue opening UART port.
    (void)HalUARTOpen( SBP_UART_PORT, &uartConfig );

    return;
}

uint16 numBytes;
//uint8 g_uart_cb_event;
/*
uart接收回调函数
当我们通过pc向开发板发送数据时，会调用该函数来接收
*/
void sbpSerialAppCallback(uint8 port, uint8 event)
{
    //uint8 pktBuffer[SBP_UART_RX_BUF_SIZE] = {0};
    //g_uart_cb_event = event;
    // unused input parameter; PC-Lint error 715.
    //(void)event;
    //HalLcdWriteString("Data form my UART:", HAL_LCD_LINE_4 );
    //返回可读的字节
    #if 0
    if ( (numBytes = Hal_UART_RxBufLen(port)) > 0 )
    {
        //读取全部有效的数据，这里可以一个一个读取，以解析特定的命令
        (void)HalUARTRead (port, pktBuffer, numBytes);
        //HalLcdWriteString(pktBuffer, HAL_LCD_LINE_5 );
        osal_set_event(sendMsgTo_TaskID, KFD_UART_EVT);
    }
    #else
    if (event & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_ABOUT_FULL | HAL_UART_RX_FULL))
    {
        #if 0
        numBytes = HalUARTRead (port, pktBuffer, SBP_UART_RX_BUF_SIZE);
        
        //SerialPrintString("UART read : ");
        HalUARTWrite (SBP_UART_PORT, pktBuffer, numBytes);
        //SerialPrintString("\r\n");
        //osal_set_event(sendMsgTo_TaskID, KFD_UART_EVT);
        osal_start_timerEx( sendMsgTo_TaskID, KFD_UART_EVT, 100 );
        #else
        //osal_start_timerEx( sendMsgTo_TaskID, KFD_UART_EVT, 50 );
        #endif
    }
    if (event & HAL_UART_TX_FULL)
    {
        ELARA_LED1 = 1;
    }
    #endif
}

void sbpSerialAppCallbackTrans(uint8 port, uint8 event)
{
    //static unsigned count=0;
  uint8  pktBuffer[SBP_UART_RX_BUF_SIZE] = {0};
  // unused input parameter; PC-Lint error 715.
 // (void)event;
 #if 0
  //返回可读的字节
  if ( (numBytes = Hal_UART_RxBufLen(port)) > 0 ){
  	//读取全部有效的数据，这里可以一个一个读取，以解析特定的命令
	(void)HalUARTRead (port, pktBuffer, numBytes);
	if (FALSE == MDUARTSerialAppSendNoti(pktBuffer,numBytes))
	{
        sprintf(pktBuffer, "Please connect me first!!!\r\n");
        HalUARTWrite (SBP_UART_PORT, pktBuffer, strlen(pktBuffer));
	}
  }
  #elif 0
  if (event & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_ABOUT_FULL | HAL_UART_RX_FULL))
  {
      uint16 len = HalUARTRead (port, pktBuffer, SBP_UART_RX_BUF_SIZE);
      if (FALSE == MDUARTSerialAppSendNoti(pktBuffer,len))
    	{
            if (len != 0)
            {
                sprintf(pktBuffer, "Please connect me first!!!\r\n");
                HalUARTWrite (SBP_UART_PORT, pktBuffer, strlen(pktBuffer));
            }
    	}
  }
  #elif 0
  if (event & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_FULL))
  {
      static uint32 old_time = 0;     //老时间
      uint32 new_time;            //新时间

      new_time = osal_GetSystemClock();
      if (new_time - old_time <= 300)
      {
        //old_time = new_time;
        return;
      }
      old_time = new_time;
      //osal_stop_timerEx(sendMsgTo_TaskID, KFD_UART_EVT);
//      osal_start_timerEx( sendMsgTo_TaskID, KFD_UART_EVT, 100 );

      if (Hal_UART_RxBufLen(port) > 0)
      {
        osal_set_event(sendMsgTo_TaskID, KFD_UART_EVT);
      }
  }
  #elif 1
    static uint32 old_time;     //老时间
    static uint32 old_time_data_len = 0;     //老时间是的数据长度    
    uint32 new_time;            //新时间
    bool ret;
    uint8 readMaxBytes = 50;
        
    if (event & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_FULL))   //串口有数据
    {
        uint8 numBytes = 0;

        numBytes = Hal_UART_RxBufLen(port);           //读出串口缓冲区有多少字节
        
        if(numBytes == 0)
        {
            //LCD_WRITE_STRING_VALUE( "ERROR: numBytes=", numBytes, 10, HAL_LCD_LINE_1 );
            old_time_data_len = 0;
            return;
        }
        if(old_time_data_len == 0)
        {
            old_time = osal_GetSystemClock(); //有数据来时， 记录一下
            old_time_data_len = numBytes;
        }
        else
        {   
            new_time = osal_GetSystemClock(); //当前时间
            
            if( (numBytes >= readMaxBytes) 
                || ( (new_time - old_time) > 20/*ms*/))
            {
                uint8 sendBytes = 0;
                uint8 *buffer = osal_mem_alloc(readMaxBytes);

                if(!buffer)
                {
                    HalUARTWrite (SBP_UART_PORT, "FAIL", 4);
                    return;
                }
                
                if(numBytes > readMaxBytes)
                {
                    sendBytes = readMaxBytes;
                }
                else
                {
                    sendBytes = numBytes;
                }

                //if(!simpleBLE_IfConnected())
                {
                    //numBytes = NpiReadBuffer(buf, sizeof(buf));
                    //NpiClearBuffer();
                     //NPI_ReadTransport(buffer,sendBytes);    //释放串口数据    
                    uint16 len = HalUARTRead (port, pktBuffer, SBP_UART_RX_BUF_SIZE);

                    if (FALSE == MDUARTSerialAppSendNoti(pktBuffer,len))
                    {
                        sprintf(buffer, "Please connect me first!!!\r\n");
                        HalUARTWrite (SBP_UART_PORT, buffer, strlen(buffer));
                    }
                }
                

                old_time = new_time;
                old_time_data_len = numBytes - sendBytes;


                osal_mem_free(buffer);
            }                
        }    
    }
  #endif
}

//uart 回调函数
static void simpleBLE_NpiSerialCallback( uint8 port, uint8 events )
{
    (void)port;

    static uint32 old_time;     //老时间
    static uint32 old_time_data_len = 0;     //老时间是的数据长度
    uint32 new_time;            //新时间
    bool ret;
    uint8 readMaxBytes = 19;
    uint8 *temp_buffer;

    if (gIsUpdatingAVR)
    {
        return;
    }
    #if 0
    if (gChuanKouTouChuanJustBegin == TRUE)
    {
        temp_buffer = osal_mem_alloc(64);
        HalUARTRead(SBP_UART_PORT, temp_buffer, 64);
        osal_mem_free(temp_buffer);
        return;
    }
    #endif
    if (events & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_FULL))   //串口有数据
    {
        (void)port;
        uint8 numBytes = 0;

        //numBytes = NPI_RxBufLen();           //读出串口缓冲区有多少字节
        numBytes = Hal_UART_RxBufLen(SBP_UART_PORT);

        if (numBytes == 0)
        {
            //LCD_WRITE_STRING_VALUE( "ERROR: numBytes=", numBytes, 10, HAL_LCD_LINE_1 );
            old_time_data_len = 0;
            return;
        }
        if (old_time_data_len == 0)
        {
            old_time = osal_GetSystemClock(); //有数据来时， 记录一下
            old_time_data_len = numBytes;
        }
        else
        {
            // 注意: 未连接上时， 有些AT 命令比较长， 所以需要开辟较大的缓冲区
            //       连接上以后， 收到每一能发送的数据不超过 MD_UART_TRANS_LEN 的字节数的限制
            //       因此，这里要限制一下
            #if 0
            if(!simpleBLE_IfConnected())
            {
                readMaxBytes = 22 ;    //这个值， 一般设置成 AT 命令中最长的字节数即可， (包含"\r\n" 计数)
            }
            else
            {
                readMaxBytes = SIMPLEPROFILE_CHAR6_LEN;
            }
            #else
            readMaxBytes = 19;
            #endif

            new_time = osal_GetSystemClock(); //当前时间

            if( (numBytes >= readMaxBytes)
                    || ( (new_time - old_time) > 20/*ms*/))
            {
                uint8 sendBytes = 0;
                uint8 *buffer = osal_mem_alloc(readMaxBytes);

                if(!buffer)
                {
                    //NPI_WriteTransport("FAIL", 4);
                    HalUARTWrite(SBP_UART_PORT, (uint8 *)"FAIL", 4);
                    return;
                }

                //
                if(numBytes > readMaxBytes)
                {
                    sendBytes = readMaxBytes;
                }
                else
                {
                    sendBytes = numBytes;
                }

                if(!Elara_IfConnected())// 如果未连接
                {
                    //numBytes = NpiReadBuffer(buf, sizeof(buf));
                    //NpiClearBuffer();
                    //NPI_ReadTransport(buffer,sendBytes);    //释放串口数据
                    HalUARTRead(SBP_UART_PORT, buffer, sendBytes);

                    if (sendBytes > 2
                            && buffer[sendBytes - 2] == '\r'
                            && buffer[sendBytes - 1] == '\n')
                    {
                        //检测到 \r\n 结束的字符串， 表明是 AT 命令
                        ret = parse_at_command(buffer, sendBytes);
                    }
                    else
                    {
                        ret = FALSE;
                    }

                    if (ret == FALSE)
                    {
                        char strTemp[12];
                        //参数错误， 直接返回 "ERROR\r\n"， 不做任何参数更改
                        sprintf(strTemp, "ERROR\r\n");
                        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, strlen(strTemp));
                    }
                }
                else // 如果是连接态
                {
                    if (simpleBLEChar6DoWrite2)
                    {
                        HalUARTRead (SBP_UART_PORT, buffer, sendBytes);
                        if (str_cmp(buffer, "AT+RESET", 8))
                        {
                            HAL_SYSTEM_RESET();
                        }
                        //simpleBLE_UartDataMain(buffer, sendBytes);
                        MDUARTSerialAppSendNoti(buffer, sendBytes);
                    }
                }

                old_time = new_time;
                old_time_data_len = numBytes - sendBytes;

                osal_mem_free(buffer);
            }
        }
    }
}

/*
打印一段数据
pBuffer可以包含0x00
*/
void sbpSerialAppWrite(uint8 *pBuffer, uint16 length)
{
    HalUARTWrite (SBP_UART_PORT, pBuffer, length);
}



#if defined(DEBUG_SERIAL)
/*
打印一个字符串
str不可以包含0x00，除非结尾
*/
void SerialPrintString(uint8 str[])
{
    HalUARTWrite (SBP_UART_PORT, str, osal_strlen((char *)str));
    KFD_HAL_DELAY(6250);
}

/*
打印指定的格式的数值
参数
title,前缀字符串
value,需要显示的数值
format,需要显示的进制，十进制为10,十六进制为16
*/

void SerialPrintValue(char *title, uint16 value, uint8 format)
{
    uint8 tmpLen;
    //uint8 buf[256];
    uint32 err;
    //uint8 pktBuffer[256] = {0};
    uint8 pktBuffer[150] = {0};
    
    tmpLen = (uint8)osal_strlen( (char *)title );
    osal_memcpy( pktBuffer, title, tmpLen );
    pktBuffer[tmpLen] = ' ';
    err = (uint32)(value);
    _ltoa( err, &pktBuffer[tmpLen + 1], format );
    strcat((char*)pktBuffer, "\r\n");
    SerialPrintString(pktBuffer);
}
#endif
