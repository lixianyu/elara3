#ifndef ELARA_FILE_CC2541_H
#define ELARA_FILE_CC2541_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * FUNCTIONS
 */
extern int elara_flash_read_init_cc2541(void);
extern void elara_flash_write_init_cc2541(void);
extern void elara_flash_read_cc2541( uint16 len, uint8 *pBuf );
extern uint32 elara_flash_if_flash_enough_cc2541(uint32 binSize);
extern int elara_flash_write_cc2541(uint16 len, uint8 *pBuf);
extern int elara_flash_write_done_cc2541(void);
extern int elara_flash_erase_cc2541(void);
extern void elara_flash_lost_connected_cc2541(void);

extern uint32 elara_flash_any_thing_else_cc2541(void);
extern bool elara_flash_if_erase_finished_cc2541(void);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
