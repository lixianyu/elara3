/*
 * avrdude - A Downloader/Uploader for AVR device programmers
 * Copyright (C) 2009 Lars Immisch
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* $Id: arduino.c 1294 2014-03-12 23:03:18Z joerg_wunsch $ */

/*
 * avrdude interface for Arduino programmer
 *
 * The Arduino programmer is mostly a STK500v1, just the signature bytes
 * are read differently.
 */

#include "ac_cfg.h"

#include <stdio.h>
#include <string.h>
//#include <unistd.h>

#include "avrdude.h"
#include "pgm.h"
#include "stk500_private.h"
#include "stk500.h"
#include "serial.h"
#include "arduino.h"
#include "..\SerialApp.h"

extern uint32 gCurPartID;
#define fprintf(s, ...)
/* read signature bytes - arduino version */
static int arduino_read_sig_bytes(PROGRAMMER *pgm, AVRPART *p, AVRMEM *m)
{
    unsigned char buf[32];

    /* Signature byte reads are always 3 bytes. */

    if (m->size < 3)
    {
        fprintf(stderr, "%s: memsize too small for sig byte read", progname);
        return -1;
    }

    buf[0] = Cmnd_STK_READ_SIGN;
    buf[1] = Sync_CRC_EOP;

    serial_send(&pgm->fd, buf, 2);

    if (serial_recv(&pgm->fd, buf, 5) < 0)
        return -1;
    if (buf[0] == Resp_STK_NOSYNC)
    {
        //fprintf(stderr, "%s: stk500_cmd(): programmer is out of sync\n",
        //		progname);
        return -1;
    }
    else if (buf[0] != Resp_STK_INSYNC)
    {
        //fprintf(stderr,
        //		"\n%s: arduino_read_sig_bytes(): (a) protocol error, "
        //		"expect=0x%02x, resp=0x%02x\n",
        //		progname, Resp_STK_INSYNC, buf[0]);
        return -2;
    }
    if (buf[4] != Resp_STK_OK)
    {
        //fprintf(stderr,
        //		"\n%s: arduino_read_sig_bytes(): (a) protocol error, "
        //		"expect=0x%02x, resp=0x%02x\n",
        //		progname, Resp_STK_OK, buf[4]);
        return -3;
    }

    m->buf[0] = buf[1];
    m->buf[1] = buf[2];
    m->buf[2] = buf[3];

    return 3;
}

//LiXianyu added. 20160803
int arduino_read_signature(unsigned char *sig)
{
    unsigned char buf[32];

    /* Signature byte reads are always 3 bytes. */

    buf[0] = Cmnd_STK_READ_SIGN;
    buf[1] = Sync_CRC_EOP;

    serial_send(NULL, buf, 2);

    if (serial_recv(NULL, buf, 5) < 0)
    {
        return -4;
    }
    if (buf[0] == Resp_STK_NOSYNC)
    {
        //fprintf(stderr, "%s: stk500_cmd(): programmer is out of sync\n",
        //		progname);
        return -1;
    }
    else if (buf[0] != Resp_STK_INSYNC)
    {
        //fprintf(stderr,
        //		"\n%s: arduino_read_sig_bytes(): (a) protocol error, "
        //		"expect=0x%02x, resp=0x%02x\n",
        //		progname, Resp_STK_INSYNC, buf[0]);
        return -2;
    }
    if (buf[4] != Resp_STK_OK)
    {
        //fprintf(stderr,
        //		"\n%s: arduino_read_sig_bytes(): (a) protocol error, "
        //		"expect=0x%02x, resp=0x%02x\n",
        //		progname, Resp_STK_OK, buf[4]);
        return -3;
    }

    sig[0] = buf[1];
    sig[1] = buf[2];
    sig[2] = buf[3];

    return 3;
}

/*
 *    ��ʱ����
 *    ����΢��
 */
static void delay_nus(uint32 timeout)
{
    while (timeout--)
    {
        asm("NOP");
        asm("NOP");
        asm("NOP");
    }
}

static int arduino_open(PROGRAMMER *pgm, char *port)
{
    int rc = 0;
    if (P0_0 == 1)
    {
        SerialPrintString("P0_0 == 1\r\n");
    }
    else
    {
        SerialPrintString("P0_0 == 0\r\n");
    }
    union pinfo pinfo;
    strcpy(pgm->port, port);
    pinfo.baud = pgm->baudrate ? pgm->baudrate : 115200;
    if (serial_open(port, pinfo, &pgm->fd) == -1)
    {
        return -1;
    }

    /* Clear DTR and RTS to unload the RESET capacitor
     * (for example in Arduino) */
    serial_set_dtr_rts(&pgm->fd, 0);
    #if 1
    if (gCurPartID == 7)
    {
        delay_nus(50000);
    }
    else
    {
        delay_nus(250000);
    }
    #else
    delay_nus(50000);
    #endif
  
    /* Set DTR and RTS back to high */
    serial_set_dtr_rts(&pgm->fd, 1);
    #if 1
    if (gCurPartID == 7)
    {
        delay_nus(50000);
    }
    else
    {
        delay_nus(250000);
    }
    #else
    delay_nus(50000);
    #endif
    /*
     * drain any extraneous input
     */
    stk500_drain(pgm, 0);
    SerialPrintString("............12\r\n");
#if 1
    if ((rc = stk500_getsync(pgm)) < 0)
    {
        //return STK_ERROR_CMD_71;
        return rc;
    }
#endif
    stk500_drain(pgm, 0);
    return 0;
}

int stk500_getsync_for_signature(void)
{
    unsigned char buf[32] = {0};
    unsigned char resp[32] = {0};
    int attempt;

    /*
     * get in sync */
    buf[0] = Cmnd_STK_GET_SYNC;
    buf[1] = Sync_CRC_EOP;

    /*
     * First send and drain a few times to get rid of line noise
     */

    serial_send(NULL, buf, 2);
    serial_drain(NULL, 0);
    serial_send(NULL, buf, 2);
    serial_drain(NULL, 0);

    for (attempt = 0; attempt < 6; attempt++)
    {
        serial_send(NULL, buf, 2);
        serial_recv(NULL, resp, 5);
        if (resp[0] == Resp_STK_INSYNC)
        {
            SerialPrintString("13............\r\n");
            break;
        }
        delay_nus(100000);
        fprintf(stderr,
                "%s: stk500_getsync() attempt %d of %d: not in sync: resp=0x%02x\n",
                progname, attempt + 1, MAX_SYNC_ATTEMPTS, resp[0]);
    }
    if (attempt == 6)
    {
        SerialPrintString("14............\r\n");
        serial_drain(NULL, 0);
        return STK_ERROR_CMD_72;
    }
#if 0
    if (serial_recv(NULL, resp, 1) < 0)
    {
        SerialPrintString("15............\r\n");
        return STK_ERROR_CMD_73;
    }
#endif
    if (resp[1] != Resp_STK_OK)
    {
        SerialPrintString("16............\r\n");
        fprintf(stderr,
                "%s: stk500_getsync(): can't communicate with device: "
                "resp=0x%02x\n",
                progname, resp[0]);
        //return STK_ERROR_CMD_74;
        return resp[1];
    }
    SerialPrintString("17............\r\n");
    serial_drain(NULL, 0);
    return 0;
}
int arduino_open_for_signature(void)
{
    int rc = 0;

    /* Clear DTR and RTS to unload the RESET capacitor
     * (for example in Arduino) */
    serial_set_dtr_rts(NULL, 0);
    delay_nus(250000);
    /* Set DTR and RTS back to high */
    serial_set_dtr_rts(NULL, 1);
    //delay_nus(50000);
    delay_nus(250000);
    /*
     * drain any extraneous input
     */
    serial_drain(NULL, 0);
    SerialPrintString("............12\r\n");
#if 1
    if ((rc = stk500_getsync_for_signature()) < 0)
    {
        //return STK_ERROR_CMD_71;
        return rc;
    }
#endif
    serial_drain(NULL, 0);
    return 0;
}

static void arduino_close(PROGRAMMER *pgm)
{
    serial_set_dtr_rts(&pgm->fd, 0);
    delay_nus(250000);
    serial_set_dtr_rts(&pgm->fd, 1);
    serial_close(&pgm->fd);
    pgm->fd.ifd = -1;
}

const char arduino_desc[] = "Arduino programmer";

void arduino_initpgm(PROGRAMMER *pgm)
{
    /* This is mostly a STK500; just the signature is read
       differently than on real STK500v1
       and the DTR signal is set when opening the serial port
       for the Auto-Reset feature */
    stk500_initpgm(pgm);

    strcpy(pgm->type, "Arduino");
    pgm->read_sig_bytes = arduino_read_sig_bytes;
    pgm->open = arduino_open;
    pgm->close = arduino_close;
}
