/*********************************************************************
 * INCLUDES
 */
#include "ac_cfg.h"

//#include <stdio.h>
#include <stdlib.h>
//#include <errno.h>
//#include <fcntl.h>
//#include <limits.h>
#include <string.h>
//#include <time.h>
//#include <unistd.h>
//#include <ctype.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <sys/time.h>
#include "OSAL.h"
#include "avr.h"
//#include "config.h"
//#include "confwin.h"
#include "fileio.h"
#include "lists.h"
//#include "par.h"
#include "pindefs.h"
//#include "term.h"
////#include "safemode.h"
#include "update.h"
#include "pgm_type.h"

#include "arduino.h"
/* Get VERSION from ac_cfg.h */
//char * version      = VERSION;
#include "..\SerialApp.h"
#include "..\elara_file_common.h"
#include "..\elara_led.h"
   
#include "main_m328p.h"
#include "main_m644p.h"
#include "main_m1284p.h"
#include "main_m128rfa1.h"
#include "main_m32u4.h"
#define fprintf(s, ...)

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static int    ovsigck;     /* 1=override sig check, 0=don't */
static PROGRAMMER  ggm;
PROGRAMMER  *pgm;
AVRPART gAvrPart;
static UPDATE g_upd;

AVRMEM g_mem0,g_mem1,g_mem2,g_mem3,g_mem4,g_mem5,g_mem6,g_mem7;
LIST g_memList;
uint8 g_flash_buf[300];
/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void initAvrPart(char* partId, AVRPART *current_part);
 
/*
 * main routine
 */
int avr_init_common(char *partId)
{
    int              rc;          /* general return code checking */
    int              exitrc;      /* exit code for main() */
    struct avrpart *p;            /* which avr part we are programming */
    UPDATE          *upd = &g_upd;

    /* options / operating mode variables */
    int     erase;       /* 1=erase chip, 0=don't */
    char   *port;        /* device port (/dev/xxx) */
    int32   baudrate;    /* override default programmer baud rate */
    double  bitclock;    /* Specify programmer bit clock (JTAG ICE) */
    int     ispdelay;    /* Specify the delay for ISP clock */
    int     safemode;    /* Enable safemode, 1=safemode on, 0=normal */
    int     silentsafe;  /* Don't ask about fuses, 1=silent, 0=normal */
    int     init_ok = 0;     /* Device initialization worked well */
    int     is_open;     /* Device open succeeded */
    enum updateflags uflags = UF_AUTO_ERASE; /* Flags for do_op() */
    unsigned char safemode_lfuse = 0xff;
    unsigned char safemode_hfuse = 0xff;
    unsigned char safemode_efuse = 0xff;
    unsigned char safemode_fuse = 0xff;

    char *safemode_response;
    int fuses_specified = 0;
    int fuses_updated = 0;

    SerialPrintString("...000\r\n");
    port          = NULL;
    erase         = 0;
    p             = NULL;
    ovsigck       = 0;
    pgm           = NULL;
    baudrate      = 0;
    bitclock      = 0.0;
    ispdelay      = 0;
    safemode      = 1;       /* Safemode on by default */
    silentsafe    = 0;       /* Ask by default */
    is_open       = 0;

    port = "HAL_UART_PORT_0";
    //-p
    //partdesc = "atmega328p";

    //-c
    //programmer = "arduino";

    //-b
    baudrate = 115200;

    //-D
    uflags &= ~UF_AUTO_ERASE;/* disable auto erase */

    //-U
    char myoptarg[22] = {0};
    //strcpy(myoptarg, "flash:w:C:\\Users\\Blink.hex:i");
    strcpy(myoptarg, "flash:w:Blink.hex:i");
    parse_op_elara(upd);
    if (upd == NULL)
    {
        //      fprintf(stderr, "%s: error parsing update operation '%s'\n", progname, optarg);
        //      exit(1);
    }

    pgm = &ggm;
    init_programmer(pgm);
    SerialPrintString("333............\r\n");
    pgm->initpgm = &arduino_initpgm;
    if (pgm->initpgm)
    {
        pgm->initpgm(pgm);
    }
    else
    {
        //fprintf(stderr,
        //        "\n%s: Can't initialize the programmer.\n\n",
        //        progname);
        //exit(1);
    }
    SerialPrintString("444............\r\n");
    if (pgm->setup)
    {
        pgm->setup(pgm);
    }
    SerialPrintString("555............\r\n");

    memset(&gAvrPart, 0, sizeof(AVRPART));
    p = &gAvrPart;
    SerialPrintString("666............\r\n");
    initAvrPart(partId, p);

    safemode = 0;

    if (avr_initmem(p) != 0)
    {
        //fprintf(stderr, "\n%s: failed to initialize memories\n",
        //        progname);
        //exit(1);
    }

    if (baudrate != 0)
    {
        //if (verbose) {
        //  fprintf(stderr, "%sOverriding Baud Rate          : %d\n", progbuf, baudrate);
        //}
        pgm->baudrate = baudrate;
    }

    if (bitclock != 0.0)
    {
        //if (verbose)
        {
            //fprintf(stderr, "%sSetting bit clk period        : %.1f\n", progbuf, bitclock);
        }
        pgm->bitclock = bitclock * 1e-6;
    }

    if (ispdelay != 0)
    {
        //if (verbose)
        {
            //fprintf(stderr, "%sSetting isp clock delay        : %3i\n", progbuf, ispdelay);
        }
        pgm->ispdelay = ispdelay;
    }

    rc = pgm->open(pgm, port);
    //return -rc;
    SerialPrintString("10............\r\n");
    if (rc < 0)
    {
        SerialPrintString("11............\r\n");
        exitrc = rc;
        pgm->ppidata = 0; /* clear all bits at exit */
        goto main_exit;
    }
    SerialPrintString("12............\r\n");
    is_open = 1;

    exitrc = 0;

    /*
     * enable the programmer
     */
    pgm->enable(pgm);

    /*
     * turn off all the status leds
     */
    pgm->rdy_led(pgm, OFF);
    pgm->err_led(pgm, OFF);
    pgm->pgm_led(pgm, OFF);
    pgm->vfy_led(pgm, OFF);

    /*
     * initialize the chip in preperation for accepting commands
     */
    init_ok = (rc = pgm->initialize(pgm, p)) >= 0;
    //return -rc;
    SerialPrintString("38............\r\n");
    if (!init_ok)
    {
        SerialPrintString("39............\r\n");
        //fprintf(stderr, "%s: initialization failed, rc=%d\n", progname, rc);
        #if 1
        if (!ovsigck)
        {
            SerialPrintString("40............\r\n");
            //fprintf(stderr, "%sDouble check connections and try again, "
            //        "or use -F to override\n"
            //        "%sthis check.\n\n",
            //        progbuf, progbuf);
            //exitrc = 1;
            exitrc = rc;
            goto main_exit;
        }
        #endif
    }
    SerialPrintString("41............\r\n");
    /* indicate ready */
    pgm->rdy_led(pgm, ON);
    
    if (init_ok && erase)
    {
        SerialPrintString("42............\r\n");
        /*
         * erase the chip's flash and eeprom memories, this is required
         * before the chip can accept new programming
         */
        if (uflags & UF_NOWRITE)
        {
            SerialPrintString("43............\r\n");
            //fprintf(stderr,
            //    "%s: conflicting -e and -n options specified, NOT erasing chip\n",
            //    progname);
        }
        else
        {
            SerialPrintString("44............\r\n");
            //if (quell_progress < 2)
            {
                //fprintf(stderr, "%s: erasing chip\n", progname);
            }
            exitrc = avr_chip_erase(pgm, p);
            if(exitrc) goto main_exit;
        }
    }
    SerialPrintString("45............\r\n");
    if (!init_ok)
    {
        SerialPrintString("46............\r\n");
        /*
         * If we came here by the -tF options, bail out now.
         */
        exitrc = 1;
        goto main_exit;
    }
    SerialPrintString("47............\r\n");

    rc = do_op(pgm, p, upd, uflags);
    //return -rc;
    exitrc = rc;
    SerialPrintString("48............\r\n");

main_exit:

    /*
     * program complete
     */

    if (is_open)
    {
        SerialPrintString("Leave avr_init()\r\n");
        pgm->powerdown(pgm);// Do nothing --- lxy added.

        rc = pgm->disable(pgm);
        if (rc < 0)
        {
            exitrc = rc;
        }

        pgm->rdy_led(pgm, OFF);

        pgm->close(pgm);
    }
    SerialPrintString("49............\r\n");
    //if (quell_progress < 2)
    {
        //fprintf(stderr, "\n%s done.  Thank you.\n\n", progname);
    }

    return exitrc;
    //return -48;
}

static void initAvrPart(char* partId, AVRPART *current_part)
{
    if (strcmp(partId, "m328p") == 0 || strcmp(partId, "m328") == 0)
    {
        initAvrPart_m328p(current_part, partId);
    }
    else if (strcmp(partId, "m644p") == 0 || strcmp(partId, "m644") == 0)
    {
        initAvrPart_m644p(current_part, partId);
    }
    else if (strcmp(partId, "m1284p") == 0 || strcmp(partId, "m1284") == 0)
    {
        initAvrPart_m1284p(current_part, partId);
    }
    else if (strcmp(partId, "m128rfa1") == 0 || strcmp(partId, "m128rfr2") == 0 ||
             strcmp(partId, "m64rfr2") == 0 || strcmp(partId, "m2560") == 0 ||
             strcmp(partId, "m2561") == 0)
    {
        initAvrPart_m128rfa1(current_part, partId);
    }
    else if (strcmp(partId, "m32u4") == 0)
    {
        initAvrPart_m32u4(current_part, partId);
    }
}

static uint8 g_blink_bin[284] = {
    0x0c,0x94,0x34,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x11,0x24,0x1f,0xbe,0xcf,0xef,0xd8,0xe0,
    0xde,0xbf,0xcd,0xbf,0x0e,0x94,0x40,0x00,0x0c,0x94,0x8b,0x00,0x0c,0x94,0x00,0x00,
    0xcf,0x93,0xdf,0x93,0x00,0xd0,0x00,0xd0,0xcd,0xb7,0xde,0xb7,0x84,0xe2,0x90,0xe0,
    0x20,0xe2,0xfc,0x01,0x20,0x83,0x85,0xe2,0x90,0xe0,0x20,0xe2,0xfc,0x01,0x20,0x83,
    0x19,0x82,0x1a,0x82,0x1b,0x82,0x1c,0x82,0x0b,0xc0,0x89,0x81,0x9a,0x81,0xab,0x81,
    0xbc,0x81,0x01,0x96,0xa1,0x1d,0xb1,0x1d,0x89,0x83,0x9a,0x83,0xab,0x83,0xbc,0x83,
    0x89,0x81,0x9a,0x81,0xab,0x81,0xbc,0x81,0x80,0x3a,0xf6,0xe8,0x9f,0x07,0xf1,0xe0,
    0xaf,0x07,0xb1,0x05,0x54,0xf3,0x85,0xe2,0x90,0xe0,0xfc,0x01,0x10,0x82,0x19,0x82,
    0x1a,0x82,0x1b,0x82,0x1c,0x82,0x0b,0xc0,0x89,0x81,0x9a,0x81,0xab,0x81,0xbc,0x81,
    0x01,0x96,0xa1,0x1d,0xb1,0x1d,0x89,0x83,0x9a,0x83,0xab,0x83,0xbc,0x83,0x89,0x81,
    0x9a,0x81,0xab,0x81,0xbc,0x81,0x80,0x3a,0xf6,0xe8,0x9f,0x07,0xf1,0xe0,0xaf,0x07,
    0xb1,0x05,0x54,0xf3,0xc0,0xcf,0xf8,0x94,0xff,0xcf,
    0xff,0xff
};
static uint8 g_blink1_bin[292] = {
    0x0c,0x94,0x34,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x11,0x24,0x1f,0xbe,0xcf,0xef,0xd8,0xe0,
    0xde,0xbf,0xcd,0xbf,0x0e,0x94,0x40,0x00,0x0c,0x94,0x8f,0x00,0x0c,0x94,0x00,0x00,
    0xcf,0x93,0xdf,0x93,0xcd,0xb7,0xde,0xb7,0x28,0x97,0x0f,0xb6,0xf8,0x94,0xde,0xbf,
    0x0f,0xbe,0xcd,0xbf,0x84,0xe2,0x90,0xe0,0x20,0xe2,0xfc,0x01,0x20,0x83,0x85,0xe2,
    0x90,0xe0,0x20,0xe2,0xfc,0x01,0x20,0x83,0x19,0x82,0x1a,0x82,0x1b,0x82,0x1c,0x82,
    0x0b,0xc0,0x89,0x81,0x9a,0x81,0xab,0x81,0xbc,0x81,0x01,0x96,0xa1,0x1d,0xb1,0x1d,
    0x89,0x83,0x9a,0x83,0xab,0x83,0xbc,0x83,0x89,0x81,0x9a,0x81,0xab,0x81,0xfc,0x81,
    0x80,0x3a,0xf6,0xe8,0x9f,0x07,0xf1,0xe0,0xaf,0x07,0xb1,0x05,0x54,0xf3,0x85,0xe2,
    0x90,0xe0,0xfc,0x01,0x10,0x82,0x1d,0x82,0x1e,0x82,0x1f,0x82,0x18,0x86,0x0b,0xc0,
    0x8d,0x81,0x9e,0x81,0xaf,0x81,0xb8,0x85,0x01,0x96,0xa1,0x1d,0xb1,0x1d,0x8d,0x83,
    0x9e,0x83,0xaf,0x83,0xb8,0x87,0x8d,0x81,0x9e,0x81,0xaf,0x81,0xb8,0x85,0x80,0x34,
    0xf2,0xe4,0x9f,0x07,0xff,0xe0,0xaf,0x07,0xb1,0x05,0x54,0xf3,0xc0,0xcf,0xf8,0x94,
    0xff,0xcf,
    0xff,0xff,
};
int justForTestFlash(void)
{
    ELARA_LED1 = 1;
    #if 1
    elara_flash_write_init();
    uint16 page_size = 128;
    uint16 block_size;
    //uint32 binSize = 292;//sizeof(g_blink1_bin);
    uint32 binSize = 282;//sizeof(g_blink_bin);
    uint32 real = ((binSize + (uint32)HAL_FLASH_WORD_SIZE - 1) / (uint32)HAL_FLASH_WORD_SIZE) * (uint32)HAL_FLASH_WORD_SIZE;//4 
    for (int i = 0; i < real; i+=block_size)
    {
        if (real - i < page_size)
        {
            block_size = real - i;
        }
        else
        {
            block_size = page_size;
        }
        elara_flash_write(block_size, g_blink_bin+i);
    }
    #endif
    //ELARA_LED1 = 0;
    return elara_flash_write_done();
    //return 0;
}
/*********************************************************************
*********************************************************************/
enum updateflags g_uflags = UF_AUTO_ERASE; /* Flags for do_op() */
int avr_init_common_1(char *partId)
{
    int              rc;          /* general return code checking */
    int              exitrc;      /* exit code for main() */
    struct avrpart *p;            /* which avr part we are programming */
    UPDATE          *upd = &g_upd;

    /* options / operating mode variables */
    int     erase;       /* 1=erase chip, 0=don't */
    char   *port;        /* device port (/dev/xxx) */
    int32   baudrate;    /* override default programmer baud rate */
    double  bitclock;    /* Specify programmer bit clock (JTAG ICE) */
    int     ispdelay;    /* Specify the delay for ISP clock */
    int     safemode;    /* Enable safemode, 1=safemode on, 0=normal */
    int     silentsafe;  /* Don't ask about fuses, 1=silent, 0=normal */
    int     init_ok = 0;     /* Device initialization worked well */
    int     is_open;     /* Device open succeeded */
    //enum updateflags uflags = UF_AUTO_ERASE; /* Flags for do_op() */
    unsigned char safemode_lfuse = 0xff;
    unsigned char safemode_hfuse = 0xff;
    unsigned char safemode_efuse = 0xff;
    unsigned char safemode_fuse = 0xff;

    char *safemode_response;
    int fuses_specified = 0;
    int fuses_updated = 0;

    SerialPrintString("...000\r\n");
    port          = NULL;
    erase         = 0;
    p             = NULL;
    ovsigck       = 0;
    pgm           = NULL;
    baudrate      = 0;
    bitclock      = 0.0;
    ispdelay      = 0;
    safemode      = 1;       /* Safemode on by default */
    silentsafe    = 0;       /* Ask by default */
    is_open       = 0;

    port = "HAL_UART_PORT_0";
    //-p
    //partdesc = "atmega328p";

    //-c
    //programmer = "arduino";

    //-b
    baudrate = 115200;

    //-D
    g_uflags &= ~UF_AUTO_ERASE;/* disable auto erase */

    //-U
    char myoptarg[22] = {0};
    //strcpy(myoptarg, "flash:w:C:\\Users\\Blink.hex:i");
    strcpy(myoptarg, "flash:w:Blink.hex:i");
    parse_op_elara(upd);
    if (upd == NULL)
    {
        //      fprintf(stderr, "%s: error parsing update operation '%s'\n", progname, optarg);
        //      exit(1);
    }

    pgm = &ggm;
    init_programmer(pgm);
    SerialPrintString("333............\r\n");
    pgm->initpgm = &arduino_initpgm;
    if (pgm->initpgm)
    {
        pgm->initpgm(pgm);
    }
    else
    {
        //fprintf(stderr,
        //        "\n%s: Can't initialize the programmer.\n\n",
        //        progname);
        //exit(1);
    }
    SerialPrintString("444............\r\n");
    if (pgm->setup)
    {
        pgm->setup(pgm);
    }
    SerialPrintString("555............\r\n");
    memset(&gAvrPart, 0, sizeof(AVRPART));
    p = &gAvrPart;
    SerialPrintString("666............\r\n");
    initAvrPart(partId, p);

    safemode = 0;

    if (avr_initmem(p) != 0)
    {
        //fprintf(stderr, "\n%s: failed to initialize memories\n",
        //        progname);
        //exit(1);
    }

    if (baudrate != 0)
    {
        //if (verbose) {
        //  fprintf(stderr, "%sOverriding Baud Rate          : %d\n", progbuf, baudrate);
        //}
        pgm->baudrate = baudrate;
    }

    if (bitclock != 0.0)
    {
        //if (verbose)
        {
            //fprintf(stderr, "%sSetting bit clk period        : %.1f\n", progbuf, bitclock);
        }
        pgm->bitclock = bitclock * 1e-6;
    }

    if (ispdelay != 0)
    {
        //if (verbose)
        {
            //fprintf(stderr, "%sSetting isp clock delay        : %3i\n", progbuf, ispdelay);
        }
        pgm->ispdelay = ispdelay;
    }

    rc = pgm->open(pgm, port);
    //return -rc;
    SerialPrintString("10............\r\n");
    if (rc < 0)
    {
        SerialPrintString("11............\r\n");
        exitrc = rc;
        pgm->ppidata = 0; /* clear all bits at exit */
        goto main_exit;
    }
    SerialPrintString("12............\r\n");
    is_open = 1;

    exitrc = 0;

    /*
     * enable the programmer
     */
    pgm->enable(pgm);

    /*
     * turn off all the status leds
     */
    pgm->rdy_led(pgm, OFF);
    pgm->err_led(pgm, OFF);
    pgm->pgm_led(pgm, OFF);
    pgm->vfy_led(pgm, OFF);

    /*
     * initialize the chip in preperation for accepting commands
     */
    init_ok = (rc = pgm->initialize(pgm, p)) >= 0;
    //return -rc;
    SerialPrintString("38............\r\n");
    if (!init_ok)
    {
        SerialPrintString("39............\r\n");
        //fprintf(stderr, "%s: initialization failed, rc=%d\n", progname, rc);
        #if 1
        if (!ovsigck)
        {
            SerialPrintString("40............\r\n");
            //fprintf(stderr, "%sDouble check connections and try again, "
            //        "or use -F to override\n"
            //        "%sthis check.\n\n",
            //        progbuf, progbuf);
            //exitrc = 1;
            exitrc = rc;
            goto main_exit;
        }
        #endif
    }
    SerialPrintString("41............\r\n");
    /* indicate ready */
    pgm->rdy_led(pgm, ON);
    
    if (init_ok && erase)
    {
        SerialPrintString("42............\r\n");
        /*
         * erase the chip's flash and eeprom memories, this is required
         * before the chip can accept new programming
         */
        if (g_uflags & UF_NOWRITE)
        {
            SerialPrintString("43............\r\n");
            //fprintf(stderr,
            //    "%s: conflicting -e and -n options specified, NOT erasing chip\n",
            //    progname);
        }
        else
        {
            SerialPrintString("44............\r\n");
            //if (quell_progress < 2)
            {
                //fprintf(stderr, "%s: erasing chip\n", progname);
            }
            exitrc = avr_chip_erase(pgm, p);
            if(exitrc) goto main_exit;
        }
    }
    SerialPrintString("45............\r\n");
    if (!init_ok)
    {
        SerialPrintString("46............\r\n");
        /*
         * If we came here by the -tF options, bail out now.
         */
        exitrc = 1;
        goto main_exit;
    }
    SerialPrintString("47............\r\n");
main_exit:
    return exitrc;
}

int avr_init_common_2(void)
{
    struct avrpart *p = &gAvrPart;            /* which avr part we are programming */
    int            rc;          /* general return code checking */
    int            exitrc = 0;      /* exit code for main() */
    UPDATE          *upd = &g_upd;
    
    rc = do_op_2(pgm, p, upd, g_uflags);
    //return -rc;
    exitrc = rc;
    SerialPrintString("48............\r\n");

main_exit:
    return exitrc;
    //return -48;
}
