/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include "bcomdef.h"
#include "OnBoard.h"
#include "hal_flash.h"
#include "osal_snv.h"
#include "elara_error.h"
#include "elara_file_spi_w25q_new.h"
#include "elara_led.h"
/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */
#define SPI_FLASH_WORD_SIZE     4

//#define SPI_FLASH_SIZE              1048576 // 8Mbit/1MB
//#define SPI_FLASH_SIZE              2097152 // 16Mbit/2MB
#define SPI_FLASH_SIZE              4194304 // 32Mbit/4MB
//#define SPI_FLASH_SIZE              8388608 // 64Mbit/8MB
//#define SPI_FLASH_SIZE              16777216 // 128Mbit/16MB

#define SPI_PAGE_SIZE               256
#define SPI_SECTOR_SIZE             4096 // 4KB
#define SPI_BLOCK_SIZE              32768 // 32KB

#define SPI_ADDRESS_BEGIN           0

// For more command: http://www.winbond.com
#define XNV_STAT_CMD  0x05
#define XNV_WREN_CMD  0x06
#define XNV_WRPG_CMD  0x02
#define XNV_READ_CMD  0x0B
//#define XNV_READ_CMD  0x03
#define XNV_CHIP_ERASE_CMD      0x60 // or 0xC7
#define XNV_SECTOR_ERASE_CMD    0x20
#define XNV_32K_BLOCK_ERASE_CMD 0x52
#define XNV_64K_BLOCK_ERASE_CMD 0xD8

#define XNV_STAT_BUSY  0x01
#define XNV_STAT_WIP   XNV_STAT_BUSY


/* ----------- XNV ---------- */
//#undef XNV_SPI_BEGIN
//#undef XNV_SPI_END

#define XNV_SPI_BEGIN()             st(P1_4 = 0;)
#define XNV_SPI_TX(x)               st(U1CSR &= ~0x02; U1DBUF = (x);)
#define XNV_SPI_RX()                U1DBUF
#define XNV_SPI_WAIT_RXRDY()        st(while (!(U1CSR & 0x02));)
#define XNV_SPI_END()               st(P1_4 = 1;)

/*
                    U1GCR    28
  (256 + U1BAUD) x 2      / 2   * 32MHz

  U1GCR.BAUD_E[4:0]
  U1BAUD.BAUD_M[7:0]
  SCK frequency = 3MHz (MBA250 max=10MHz, CC254x max = 4MHz)
    U1GCR |= 0x10;
    U1BAUD = 0x80;
  SCK frequency = 115200
    U1GCR = 12;
    U1BAUD = 216;
*/
// The TI reference design uses UART1 Alt. 2 in SPI mode.
#if 0
#define XNV_SPI_INIT_ELARA() \
st( \
  /* Mode select UART1 SPI Mode as master. */\
  U1CSR = 0; \
  \
  /* Setup for 115200 baud. */\
  U1GCR = 12; \
  U1BAUD = 216; \
  \
  /* Set bit order to MSB */\
  U1GCR |= BV(5); \
  \
  /* Set UART1 I/O to alternate 2 location on P1 pins. */\
  PERCFG |= 0x02;  /* U1CFG */\
  \
  /* Select peripheral function on I/O pins but SS is left as GPIO for separate control. */\
  P1SEL |= 0xE0;  /* SELP1_[7:4] */\
  /* P1.1,2,3: reset, LCD CS, XNV CS. */\
  P1SEL &= ~0x0E; \
  P1 |= 0x0E; \
  P1_1 = 0; \
  P1DIR |= 0x0E; \
  \
  /* Give UART1 priority over Timer3. */\
  P2SEL &= ~0x20;  /* PRI2P1 */\
  \
  /* When SPI config is complete, enable it. */\
  U1CSR |= 0x40; \
  /* Release XNV reset. */\
  P1_1 = 1; \
)
#elif 1
#define XNV_SPI_INIT_ELARA() \
st( \
  /* Mode select UART1 SPI Mode as master. */\
  U1CSR = 0; \
  \
  /* Setup for 115200 baud. */\
  U1GCR = 12; \
  U1BAUD = 216; \
  \
  /* Set bit order to MSB */\
  U1GCR |= BV(5); \
  \
  /* Set UART1 I/O to alternate 2 location on P1 pins. */\
  PERCFG |= 0x02;  /* U1CFG */\
  \
  /* Select peripheral function on I/O pins but SS is left as GPIO for separate control. */\
  P1SEL |= 0xE0;  /* SELP1_[7:4] */\
  /* P1.1,2,3: reset, LCD CS, XNV CS. */\
  P1SEL &= ~0x0E; \
  P1 |= 0x0E; \
  P1_1 = 0; \
  P1DIR |= 0x0E; \
  \
  /* Give UART1 priority over Timer3. */\
  P2SEL &= ~0x20;  /* PRI2P1 */\
  \
  /* When SPI config is complete, enable it. */\
  U1CSR |= 0x40; \
  /* Release XNV reset. */\
  P1_1 = 1; \
)

#define XNV_SPI_ERASE_INIT_ELARA() \
st( \
  /* Mode select UART1 SPI Mode as master. */\
  U1CSR = 0; \
  \
  /* Setup for 3MHz. */\
  U1GCR |= 0x10; \
  U1BAUD = 0x80; \
  \
  /* Set bit order to MSB */\
  U1GCR |= BV(5); \
  \
  /* Set UART1 I/O to alternate 2 location on P1 pins. */\
  PERCFG |= 0x02;  /* U1CFG */\
  \
  /* Select peripheral function on I/O pins but SS is left as GPIO for separate control. */\
  P1SEL |= 0xE0;  /* SELP1_[7:4] */\
  /* P1.1,2,3: reset, LCD CS, XNV CS. */\
  P1SEL &= ~0x0E; \
  P1 |= 0x0E; \
  P1_1 = 0; \
  P1DIR |= 0x0E; \
  \
  /* Give UART1 priority over Timer3. */\
  P2SEL &= ~0x20;  /* PRI2P1 */\
  \
  /* When SPI config is complete, enable it. */\
  U1CSR |= 0x40; \
  /* Release XNV reset. */\
  P1_1 = 1; \
)
#else
#define XNV_SPI_INIT_ELARA() \
st( \
  /* Mode select UART1 SPI Mode as master. */\
  U1CSR = 0; \
  \
  /* Setup for 115200 baud. */\
  U1GCR = 15; \
  U1BAUD = 255; \
  \
  /* Set bit order to MSB */\
  U1GCR |= BV(5); \
  \
  /* Set UART1 I/O to alternate 2 location on P1 pins. */\
  PERCFG |= 0x02;  /* U1CFG */\
  \
  /* Select peripheral function on I/O pins but SS is left as GPIO for separate control. */\
  P1SEL |= 0xE0;  /* SELP1_[7:4] */\
  /* P1.1,2,3: reset, LCD CS, XNV CS. */\
  P1SEL &= ~0x0E; \
  P1 |= 0x0E; \
  P1_1 = 0; \
  P1DIR |= 0x0E; \
  \
  /* Give UART1 priority over Timer3. */\
  P2SEL &= ~0x20;  /* PRI2P1 */\
  \
  /* When SPI config is complete, enable it. */\
  U1CSR |= 0x40; \
  /* Release XNV reset. */\
  P1_1 = 1; \
)
#endif
/*********************************************************************
 * LOCAL VARIABLES
 */
static uint32 g_AddressBegin = 0;
static uint32 g_AddressActive = 0;
static uint32 g_AddressActiveRead = 0;

uint32 g_bin_size = 0;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void xnvSPIWrite(uint8 ch)
{
    XNV_SPI_TX(ch);
    XNV_SPI_WAIT_RXRDY();
}

static void HalSPIRead(uint32 addr, uint8 *pBuf, uint16 len)
{

    uint8 shdw = P1DIR;
    P1DIR |= BV(3);

    XNV_SPI_BEGIN();
    do
    {
        xnvSPIWrite(XNV_STAT_CMD);
    } while (XNV_SPI_RX() & XNV_STAT_WIP);
    XNV_SPI_END();
    asm("NOP");
    asm("NOP");

    XNV_SPI_BEGIN();
    xnvSPIWrite(XNV_READ_CMD);
    xnvSPIWrite(addr >> 16);
    xnvSPIWrite(addr >> 8);
    xnvSPIWrite(addr);
    xnvSPIWrite(0);

    while (len--)
    {
        xnvSPIWrite(0);
        *pBuf++ = XNV_SPI_RX();
    }
    XNV_SPI_END();

    P1DIR = shdw;
}

static void HalSPIWrite(uint32 addr, uint8 *pBuf, uint16 len)
{
    uint8 cnt;
    uint8 shdw = P1DIR;
    P1DIR |= BV(3);
    while (len)
    {
        XNV_SPI_BEGIN();
        do
        {
            xnvSPIWrite(XNV_STAT_CMD);
        } while (XNV_SPI_RX() & XNV_STAT_WIP);
        XNV_SPI_END();
        asm("NOP");
        asm("NOP");

        XNV_SPI_BEGIN();
        xnvSPIWrite(XNV_WREN_CMD);
        XNV_SPI_END();
        asm("NOP");
        asm("NOP");

        XNV_SPI_BEGIN();
        xnvSPIWrite(XNV_WRPG_CMD);
        xnvSPIWrite(addr >> 16);
        xnvSPIWrite(addr >> 8);
        xnvSPIWrite(addr);

        // Can only write within any one page boundary, so prepare for next page write if bytes remain.
        cnt = 0 - (uint8)addr;
        if (cnt)
        {
            addr += cnt;
        }
        else
        {
            addr += 256;
        }

        do
        {
            xnvSPIWrite(*pBuf++);
            cnt--;
            len--;
        }
        while (len && cnt);
        XNV_SPI_END();
    }
    P1DIR = shdw;
}

int HalSPIEraseChip(void)
{
    //XNV_SPI_INIT_ELARA();

    uint8 shdw = P1DIR;
    P1DIR |= BV(3);
    XNV_SPI_BEGIN();
    do
    {
        xnvSPIWrite(XNV_STAT_CMD);
    }
    while (XNV_SPI_RX() & 0x01);
    XNV_SPI_END();
    asm("NOP");
    asm("NOP");

    XNV_SPI_BEGIN();
    //do
    {
        xnvSPIWrite(XNV_WREN_CMD);
    } 
    //while (!(XNV_SPI_RX() & 0x02));
    XNV_SPI_END();
    asm("NOP");
    asm("NOP");

    XNV_SPI_BEGIN();
    //xnvSPIWrite(0x60);
    xnvSPIWrite(0xC7);
    XNV_SPI_END();

    P1DIR = shdw;

    return 2;
}

//The sector size is 4KB
int HalSPIEraseSector(void)
{
    return 0;
}

int HalSPIErase32KBBlock(void)
{
    return 0;
}

int HalSPIErase64KBBlock(void)
{
    static uint32 addr = 0;
//    uint8 shdw = P1DIR;
//    P1DIR |= BV(3);
    XNV_SPI_BEGIN();
    do
    {
        xnvSPIWrite(XNV_STAT_CMD);
    }
    while (XNV_SPI_RX() & 0x01);
    XNV_SPI_END();
    asm("NOP");
    asm("NOP");

    XNV_SPI_BEGIN();
    xnvSPIWrite(XNV_WREN_CMD);
    XNV_SPI_END();
    asm("NOP");
    asm("NOP");

    XNV_SPI_BEGIN();
    xnvSPIWrite(0xD8);
    xnvSPIWrite(addr >> 16);
    xnvSPIWrite(addr >> 8);
    xnvSPIWrite(addr);
    XNV_SPI_END();

    //P1DIR = shdw;

    addr += 65536;
    if (addr >= 16777216)
    {
        return 0;
    }
    return 1;
}

void HalSPIEraseBlockInit(void)
{
    XNV_SPI_INIT_ELARA();
}

bool HalSPIEraseChipFinished(void)
{
    uint8 shdw = P1DIR;
    P1DIR |= BV(3);
    XNV_SPI_BEGIN();
    xnvSPIWrite(XNV_STAT_CMD);
    uint8 rxval = XNV_SPI_RX();
    XNV_SPI_END();

    P1DIR = shdw;
    if (rxval & 0x01)
    {
        return FALSE;
    }
    return TRUE;
}
/**************************************************************************************************
软件延时
这里要注意了，若使用uint32类型，时间将延长5倍！！！
如
void HalHW_WaitUs(uint32 microSecs)
 **************************************************************************************************/
void HalHW_WaitUs(uint16 microSecs)
{
    while(microSecs--)
    {
        /* 3 NOPs == 1 usecs */
        asm("NOP");
        asm("NOP");
        asm("NOP");

    }
}

void HalHW_WaitMS(uint16 ms)
{
    while(ms--)
    {
        int i = 0;
        for(i = 20; i > 0; i--)
            HalHW_WaitUs(50);
    }
}

static uint8 buf[8] = "1234";
//uint8 bufrx[10];
int test_spi_get_status_register2(uint8 *pBuf, uint8 len)
{
    XNV_SPI_INIT_ELARA();

    //HalSPIWrite(0x0, buf, 5);
    //注意，连续读写之间至少要延时800us
    //  HalHW_WaitUs(800);
    HalSPIRead(0x0, pBuf, 8);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////
int elara_flash_read_init_spi_w25q(void)
{
    XNV_SPI_INIT_ELARA();
    g_AddressActiveRead = g_AddressBegin;
    return 0;
}

static bool ifFirst = TRUE;
void elara_flash_write_init_spi_w25q(void)
{
    ifFirst = TRUE;
    uint8 ret;
    ret = elara_snv_read(&g_AddressActive);
    if (ret != SUCCESS)
    {
        g_AddressActive = SPI_ADDRESS_BEGIN;
    }

    g_AddressBegin = g_AddressActive;
    g_bin_size = 0;
}

/*********************************************************************
 * @fn      elara_flash_if_flash_enough
 *
 * @brief   Before write bin to flash, we should check flash memory big enough.
            (Should run elara_flash_write_init function first.)
 *
 * @param   binSize - The size of the bin
 *
 * @return  The aviliabel flash size, 0 fail.
 */
uint32 elara_flash_if_flash_enough_spi_w25q(uint32 binSize)
{
    uint32 allFlashSize;

    // The aviliable flash size now.
    uint32 flashsizeAV = (uint32)SPI_FLASH_SIZE - g_AddressActive;

    // 4 bytes align
    uint32 real = ((binSize + (uint32)SPI_FLASH_WORD_SIZE - 1) / (uint32)SPI_FLASH_WORD_SIZE) * (uint32)SPI_FLASH_WORD_SIZE;//4

    if (SPI_FLASH_SIZE < g_AddressActive)
    {
        goto enoughExits;
    }
    if (real < flashsizeAV)
    {
        return flashsizeAV;
    }
enoughExits:
    ELARA_LED2 = 1;
    ELARA_LED1 = 1;
    allFlashSize = SPI_FLASH_SIZE;
    if (real < allFlashSize)
    {
        return 0xFFFFFFFF;
    }
    return 0;
}

int elara_flash_erase_spi_w25q(void)
{
    XNV_SPI_INIT_ELARA();
    g_AddressActive = SPI_ADDRESS_BEGIN;
    g_AddressBegin = g_AddressActive;
    #if 0
    elara_snv_write(BLE_NVID_ADR_BEGIN, 4, &g_AddressBegin);
    elara_snv_write(BLE_NVID_ADR_ACTIVE, 4, &g_AddressActive);
    #endif
    HalSPIEraseChip();
    return 1;
}

bool elara_flash_if_erase_finished_spi_w25q(void)
{
    uint8 shdw = P1DIR;
    P1DIR |= BV(3);
    XNV_SPI_BEGIN();
    xnvSPIWrite(XNV_STAT_CMD);
    uint8 rxval = XNV_SPI_RX();
    XNV_SPI_END();

    P1DIR = shdw;
    if (rxval & 0x01)
    {
        return FALSE;
    }
    return TRUE;
}

/*
 *    延时函数
 *    输入微妙
 */
static void delay_nus(uint32 timeout)
{
    while (timeout--)
    {
        asm("NOP");
        asm("NOP");
        asm("NOP");
    }
}

bool elara_flash_if_first_256_FF_spi_w25q(void)
{
    XNV_SPI_INIT_ELARA();
    bool flag1 = FALSE;
    bool flag2 = FALSE;
    uint8 allFF[128];
    memset(allFF, 0xFF, sizeof(allFF));
    uint8 first256buffer[128];
    memset(first256buffer, 0, sizeof(first256buffer));
    HalSPIRead(0x00000000, first256buffer, sizeof(first256buffer));
    if (memcmp(first256buffer, allFF, sizeof(allFF)) == 0)
    {
        flag1 = TRUE;
        //return TRUE;
    }
    delay_nus(800);
    memset(first256buffer, 0, sizeof(first256buffer));
    HalSPIRead(0x00000000, first256buffer, sizeof(first256buffer));
    if (memcmp(first256buffer, allFF, sizeof(allFF)) == 0)
    {
        flag2 = TRUE;
        //return TRUE;
    }
    if (flag1 == TRUE && flag2 == TRUE)
    {
        return TRUE;
    }
    return FALSE;
}

/*********************************************************************
 * @fn      elara_flash_read
 *
 * @brief   Read data from flash.
 *
 * @param   len - len%256 = 0
 * @param   pBuf - Data is read into this buffer.
 *
 * @return
 */
void elara_flash_read_spi_w25q( uint16 len, uint8 *pBuf )
{
    HalSPIRead(g_AddressActiveRead, pBuf, len);
    g_AddressActiveRead += len;
    //HalHW_WaitUs(800);
}

/*********************************************************************
 * @fn      elara_flash_write
 *
 * @brief   Writes 'len' bytes to the internal flash.
 *
 * @param   len - len%4 must be zero and len must < HAL_FLASH_PAGE_SIZE
 * @param   pBuf - Valid buffer space at least as big as 'len' X 4.
 *
 * @return
 */
int elara_flash_write_spi_w25q(uint16 len, uint8 *pBuf)
{
    #if 1
    if (ifFirst == TRUE)
    {
        XNV_SPI_INIT_ELARA();
        ifFirst = FALSE;
    }
    #endif
    HalSPIWrite(g_AddressActive, pBuf, len);
    g_bin_size += len;
    
    g_AddressActive += len;
    //HalHW_WaitUs(800);
    return 0;
}

int elara_flash_write_done_spi_w25q(void)
{
    // SPI_PAGE_SIZE bytes align.
    uint32 real = ((g_AddressActive + (uint32)SPI_PAGE_SIZE - 1) / (uint32)SPI_PAGE_SIZE) * (uint32)SPI_PAGE_SIZE; //4
    elara_snv_write(&real);
    return 0;
}

// When lost connection or upgrade fail, we should save current active page and offset.
void elara_flash_lost_connected_spi_w25q(void)
{
    // SPI_PAGE_SIZE bytes align.
    uint32 real = ((g_AddressActive + (uint32)SPI_PAGE_SIZE - 1) / (uint32)SPI_PAGE_SIZE) * (uint32)SPI_PAGE_SIZE; //4
    elara_snv_write(&real);
}

uint32 elara_flash_any_thing_else_spi_w25q(void)
{
    uint8 ret;
    ret = elara_snv_read(&g_AddressActive);
    if (ret != SUCCESS)
    {
        g_AddressActive = SPI_ADDRESS_BEGIN;
    }

    g_AddressBegin = g_AddressActive;
    g_bin_size = 0;
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
#if 1
static uint8 g_activePg;
static uint16 g_offset;
#define STORE_PAGE_BEGIN  86
//Should be 124
//#define STORE_PAGE_END (HAL_NV_PAGE_BEG-1)
#define STORE_PAGE_END   STORE_PAGE_BEGIN
#define OFFSET_END       HAL_FLASH_PAGE_SIZE
#define STEP_COUNT       4
uint8 elara_snv_init(void)
{
    g_activePg = STORE_PAGE_BEGIN;
    g_offset = 0;
    uint8 pg;
    uint16 offset = 0;
    uint32 ret[1];
    uint32 nothingsFF[1] = {0xFFFFFFFF};
    bool ifGetOne = FALSE;
    for (pg = STORE_PAGE_BEGIN; pg <= STORE_PAGE_END; pg++)
    {
        for (offset = 0; offset < OFFSET_END; offset+=STEP_COUNT)
        {
            HalFlashRead(pg, offset, (uint8*)&ret, STEP_COUNT);
            if (memcmp( ret, nothingsFF, STEP_COUNT) == 0)
            {
                g_activePg = pg;
                g_offset = offset;
                ifGetOne = TRUE;
                break;
            }
        }
        if (ifGetOne == TRUE)
        {
            break;
        }
    }
    if (ifGetOne == FALSE)
    {
        uint8 page = STORE_PAGE_END;
        uint16 offset = OFFSET_END - STEP_COUNT;
        uint32 rets[1];
        HalFlashRead(page, offset, (uint8 *)rets, STEP_COUNT);
        #if 1
        uint8 i;
        for (i = STORE_PAGE_BEGIN; i <= STORE_PAGE_END; i++)
        {
            HalFlashErase(i);//Flash-page erase time: 20 ms
        }
        #endif
        uint16 addr = (g_offset >> 2) + ((uint16)g_activePg << 9);
        HalFlashWrite(addr, (uint8 *)rets, 1);
        g_offset = STEP_COUNT;
    }
    return 0;
}

/*
 * pBuf length must be 4 bytes
 */
uint8 elara_snv_read( void *pBuf )
{
    uint32 ret = 0;
    uint16 realOffset = g_offset;
    uint8 realActivePg = g_activePg;
    if (g_offset == 0)
    {
        if (g_activePg == STORE_PAGE_BEGIN)
        {
            HalFlashRead(g_activePg, g_offset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
        }
        else
        {
            realOffset = OFFSET_END - STEP_COUNT;
            realActivePg--;
            HalFlashRead(realActivePg, realOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
        }
    }
    else
    {
        realOffset -= STEP_COUNT;
        HalFlashRead(g_activePg, realOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
    }
    if (ret == 0xFFFFFFFF)
    {
        return NV_OPER_FAILED;
    }
    *((uint32*)pBuf) = ret;

    return SUCCESS;
}

/*
 * pBuf length must be 4 bytes
 */
uint8 elara_snv_write( void *pBuf )
{
    uint16 realOffset = g_offset;
    if (realOffset >= OFFSET_END)
    {
        HalFlashErase(STORE_PAGE_BEGIN);
        realOffset = 0;
    }
    uint16 addr = (realOffset >> 2) + ((uint16)g_activePg << 9);
    HalFlashWrite(addr, pBuf, 1);

    g_offset = realOffset + STEP_COUNT;
    return 0;
}
#endif

#if 1
static uint8 g_activePgUState;
static uint16 g_offsetUState;
#define STORE_PAGE_BEGIN_U_STATE  88
//Should be 124
//#define STORE_PAGE_END (HAL_NV_PAGE_BEG-1)
#define STORE_PAGE_END_U_STATE   STORE_PAGE_BEGIN_U_STATE
#define OFFSET_END_U_STATE       HAL_FLASH_PAGE_SIZE
#define STEP_COUNT_U_STATE       4
uint8 elara_snv_init_u_state(void)
{
    g_activePgUState = STORE_PAGE_BEGIN_U_STATE;
    g_offsetUState = 0;
    uint8 pg;
    uint16 offset = 0;
    uint32 ret[1];
    uint32 nothingsFF[1] = {0xFFFFFFFF};
    bool ifGetOne = FALSE;
    for (pg = STORE_PAGE_BEGIN_U_STATE; pg <= STORE_PAGE_END_U_STATE; pg++)
    {
        for (offset = 0; offset < OFFSET_END_U_STATE; offset+=STEP_COUNT_U_STATE)
        {
            HalFlashRead(pg, offset, (uint8*)&ret, STEP_COUNT_U_STATE);
            if (memcmp( ret, nothingsFF, STEP_COUNT_U_STATE) == 0)
            {
                g_activePgUState = pg;
                g_offsetUState = offset;
                ifGetOne = TRUE;
                break;
            }
        }
        if (ifGetOne == TRUE)
        {
            break;
        }
    }
    if (ifGetOne == FALSE)
    {
        uint8 page = STORE_PAGE_END_U_STATE;
        uint16 offset = OFFSET_END_U_STATE - STEP_COUNT_U_STATE;
        uint32 rets[1];
        HalFlashRead(page, offset, (uint8 *)rets, STEP_COUNT_U_STATE);
        #if 1
        uint8 i;
        for (i = STORE_PAGE_BEGIN_U_STATE; i <= STORE_PAGE_END_U_STATE; i++)
        {
            HalFlashErase(i);//Flash-page erase time: 20 ms
        }
        #endif
        uint16 addr = (g_offsetUState >> 2) + ((uint16)g_activePgUState << 9);
        HalFlashWrite(addr, (uint8 *)rets, 1);
        g_offsetUState = STEP_COUNT_U_STATE;
    }
    return 0;
}

/*
 * pBuf length must be 4 bytes
 */
uint8 elara_snv_read_u_state( void *pBuf )
{
    uint32 ret = 0;
    uint16 realOffset = g_offsetUState;
    uint8 realActivePg = g_activePgUState;
    if (g_offsetUState == 0)
    {
        if (g_activePgUState == STORE_PAGE_BEGIN_U_STATE)
        {
            HalFlashRead(g_activePgUState, g_offsetUState, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
        }
        else
        {
            realOffset = OFFSET_END_U_STATE - STEP_COUNT_U_STATE;
            realActivePg--;
            HalFlashRead(realActivePg, realOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
        }
    }
    else
    {
        realOffset -= STEP_COUNT_U_STATE;
        HalFlashRead(g_activePgUState, realOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
    }
    if (ret == 0xFFFFFFFF)
    {
        return NV_OPER_FAILED;
    }
    *((uint32*)pBuf) = ret;

    return SUCCESS;
}

/*
 * pBuf length must be 4 bytes
 */
uint8 elara_snv_write_u_state( void *pBuf )
{
    uint16 realOffset = g_offsetUState;
    if (realOffset >= OFFSET_END_U_STATE)
    {
        HalFlashErase(STORE_PAGE_BEGIN_U_STATE);
        realOffset = 0;
    }
    uint16 addr = (realOffset >> 2) + ((uint16)g_activePgUState << 9);
    HalFlashWrite(addr, pBuf, 1);

    g_offsetUState = realOffset + STEP_COUNT_U_STATE;
    return 0;
}
#endif

//For record partID.
#if 1
static uint8 g_activePgPartID;
static uint16 g_offsetPartID;
#define STORE_PAGE_BEGIN_PART_ID  90
//Should be 124
//#define STORE_PAGE_END (HAL_NV_PAGE_BEG-1)
#define STORE_PAGE_END_PART_ID   STORE_PAGE_BEGIN_PART_ID
#define OFFSET_END_PART_ID       HAL_FLASH_PAGE_SIZE
#define STEP_COUNT_PART_ID       4
uint8 elara_snv_init_part_id(void)
{
    g_activePgPartID = STORE_PAGE_BEGIN_PART_ID;
    g_offsetPartID = 0;
    uint8 pg;
    uint16 offset = 0;
    uint32 ret[1];
    uint32 nothingsFF[1] = {0xFFFFFFFF};
    bool ifGetOne = FALSE;
    for (pg = STORE_PAGE_BEGIN_PART_ID; pg <= STORE_PAGE_END_PART_ID; pg++)
    {
        for (offset = 0; offset < OFFSET_END_PART_ID; offset+=STEP_COUNT_PART_ID)
        {
            HalFlashRead(pg, offset, (uint8*)&ret, STEP_COUNT_PART_ID);
            if (memcmp( ret, nothingsFF, STEP_COUNT_PART_ID) == 0)
            {
                g_activePgPartID = pg;
                g_offsetPartID = offset;
                ifGetOne = TRUE;
                break;
            }
        }
        if (ifGetOne == TRUE)
        {
            break;
        }
    }
    if (ifGetOne == FALSE)
    {
        uint8 page = STORE_PAGE_END_PART_ID;
        uint16 offset = OFFSET_END_PART_ID - STEP_COUNT_PART_ID;
        uint32 rets[1];
        HalFlashRead(page, offset, (uint8 *)rets, STEP_COUNT_PART_ID);
        #if 1
        uint8 i;
        for (i = STORE_PAGE_BEGIN_PART_ID; i <= STORE_PAGE_END_PART_ID; i++)
        {
            HalFlashErase(i);//Flash-page erase time: 20 ms
        }
        #endif
        uint16 addr = (g_offsetPartID >> 2) + ((uint16)g_activePgPartID << 9);
        HalFlashWrite(addr, (uint8 *)rets, 1);
        g_offsetPartID = STEP_COUNT_PART_ID;
    }
    return 0;
}

/*
 * pBuf length must be 4 bytes
 */
uint8 elara_snv_read_part_id( void *pBuf )
{
    uint32 ret = 0;
    uint16 realOffset = g_offsetPartID;
    uint8 realActivePg = g_activePgPartID;
    if (g_offsetPartID == 0)
    {
        if (g_activePgPartID == STORE_PAGE_BEGIN_PART_ID)
        {
            HalFlashRead(g_activePgPartID, g_offsetPartID, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
        }
        else
        {
            realOffset = OFFSET_END_PART_ID - STEP_COUNT_PART_ID;
            realActivePg--;
            HalFlashRead(realActivePg, realOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
        }
    }
    else
    {
        realOffset -= STEP_COUNT_PART_ID;
        HalFlashRead(g_activePgPartID, realOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
    }
    if (ret == 0xFFFFFFFF)
    {
        return NV_OPER_FAILED;
    }
    *((uint32*)pBuf) = ret;

    return SUCCESS;
}

/*
 * pBuf length must be 4 bytes
 */
uint8 elara_snv_write_part_id( void *pBuf )
{
    uint16 realOffset = g_offsetPartID;
    if (realOffset >= OFFSET_END_PART_ID)
    {
        HalFlashErase(STORE_PAGE_BEGIN_PART_ID);
        realOffset = 0;
    }
    uint16 addr = (realOffset >> 2) + ((uint16)g_activePgPartID << 9);
    HalFlashWrite(addr, pBuf, 1);

    g_offsetPartID = realOffset + STEP_COUNT_PART_ID;
    return 0;
}
#endif

/* For bond rate store...
 * One variable one page! This makes things easy.
 */
#if 1
static uint8 g_activePage;
static uint16 g_CurOffset;
#define STORE_PAGE_BEGIN_BOND  92 //Should be different to STORE_PAGE_BEGIN
#define STORE_PAGE_END_BOND   STORE_PAGE_BEGIN_BOND
#define OFFSET_END_BOND       HAL_FLASH_PAGE_SIZE
#define STEP_COUNT_BOND       4
uint8 elara_snv_bond_init(void)
{
    g_activePage = STORE_PAGE_BEGIN_BOND;
    g_CurOffset = 0;
    uint8 pg;
    uint16 offset = 0;
    uint32 ret[1];
    uint32 nothingsFF[1] = {0xFFFFFFFF};
    bool ifGetOne = FALSE;
    for (pg = STORE_PAGE_BEGIN_BOND; pg <= STORE_PAGE_END_BOND; pg++)
    {
        for (offset = 0; offset < OFFSET_END_BOND; offset+=STEP_COUNT_BOND)
        {
            HalFlashRead(pg, offset, (uint8*)&ret, STEP_COUNT_BOND);
            if (memcmp( ret, nothingsFF, STEP_COUNT_BOND ) == 0)
            {
                g_activePage = pg;
                g_CurOffset = offset;
                ifGetOne = TRUE;
                break;
            }
        }
        if (ifGetOne == TRUE)
        {
            break;
        }
    }
    if (ifGetOne == FALSE)
    {
        uint8 page = STORE_PAGE_END_BOND;
        uint16 offset = OFFSET_END_BOND - STEP_COUNT_BOND;
        uint32 rets[1];
        HalFlashRead(page, offset, (uint8 *)rets, STEP_COUNT_BOND);
        #if 1
        uint8 i;
        for (i = STORE_PAGE_BEGIN_BOND; i <= STORE_PAGE_END_BOND; i++)
        {
            HalFlashErase(i);//Flash-page erase time: 20 ms
        }
        #endif
        uint16 addr = (g_CurOffset >> 2) + ((uint16)g_activePage << 9);
        HalFlashWrite(addr, (uint8 *)rets, 1);
        g_CurOffset = STEP_COUNT_BOND;
        #if 0
        if (rets[0] == 1)
        {
            uint16 offset = g_CurOffset + 4;
            uint16 addr = (offset >> 2) + ((uint16)g_activePage << 9);
            HalFlashWrite(addr, (uint8 *)&rets[1], 4);
        }
        #endif
    }
    else
    {
        #if 0
        uint8 real_activePg;
        uint16 real_offset;
        if (g_CurOffset == 0)
        {
            if (g_activePage == STORE_PAGE_BEGIN_BOND)
            {
                return 0;
            }
            else
            {
                real_activePg = g_activePage - 1;
                real_offset = OFFSET_END_BOND - STEP_COUNT_BOND;
            }
        }
        else // g_offset > 0
        {
            real_activePg = g_activePage;
            real_offset = g_CurOffset - STEP_COUNT;
        }
        uint32 uState = 0xFFFFFFFF;
        HalFlashRead(real_activePg, real_offset, (uint8 *)&uState, 4);
        if (uState == 1)
        {
            uint32 rets[4];
            HalFlashRead(real_activePg, real_offset+4, (uint8 *)rets, 16);
            uint16 offset = g_CurOffset + 4;
            uint16 addr = (offset >> 2) + ((uint16)g_activePage << 9);
            HalFlashWrite(addr, (uint8 *)rets, 4);
        }
        #endif
    }
    return 0;
}

/*
 * pBuf length must be 4 bytes
 */
uint8 elara_snv_bond_read( void *pBuf )
{
    uint32 ret = 0;
    uint16 realOffset = g_CurOffset;
    uint8 realActivePg = g_activePage;
    if (g_CurOffset == 0)
    {
        if (g_activePage == STORE_PAGE_BEGIN_BOND)
        {
            HalFlashRead(g_activePage, g_CurOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
        }
        else
        {
            realOffset = OFFSET_END_BOND- STEP_COUNT_BOND;
            realActivePg--;
            HalFlashRead(realActivePg, realOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
        }
    }
    else
    {
        realOffset -= STEP_COUNT_BOND;
        HalFlashRead(g_activePage, realOffset, (uint8 *)&ret, HAL_FLASH_WORD_SIZE);
    }
    if (ret == 0xFFFFFFFF)
    {
        return NV_OPER_FAILED;
    }
    *((uint32*)pBuf) = ret;

    return SUCCESS;
}

/*
 * pBuf length must be 4 bytes
 */
uint8 elara_snv_bond_write( void *pBuf )
{
    uint16 realOffset = g_CurOffset;
    if (realOffset >= OFFSET_END_BOND)
    {
        HalFlashErase(STORE_PAGE_BEGIN_BOND);
        realOffset = 0;
    }
    uint16 addr = (realOffset >> 2) + ((uint16)g_activePage << 9);
    HalFlashWrite(addr, pBuf, 1);

    g_CurOffset = realOffset + STEP_COUNT_BOND;
    return 0;
}
#endif
