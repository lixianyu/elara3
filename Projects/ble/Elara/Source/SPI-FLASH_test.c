/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "OnBoard.h"
#include "osal_snv.h"
#include "elara_error.h"
/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */
#define SPI_FLASH_WORD_SIZE     4

//#define SPI_FLASH_SIZE              1048576 // 8Mbit/1MB
//#define SPI_FLASH_SIZE              2097152 // 16Mbit/2MB
#define SPI_FLASH_SIZE              4194304 // 32Mbit/4MB
//#define SPI_FLASH_SIZE              8388608 // 64Mbit/8MB
//#define SPI_FLASH_SIZE              16777216 // 128Mbit/16MB

#define SPI_PAGE_SIZE               256
#define SPI_SECTOR_SIZE             4096 // 4KB
#define SPI_BLOCK_SIZE              32768 // 32KB

#define SPI_ADDRESS_BEGIN           0

// For more command: http://www.winbond.com
#define XNV_STAT_CMD  0x05
#define XNV_WREN_CMD  0x06
#define XNV_WRPG_CMD  0x02
#define XNV_READ_CMD  0x0B
//#define XNV_READ_CMD  0x03
#define XNV_CHIP_ERASE_CMD      0x60 // or 0xC7
#define XNV_SECTOR_ERASE_CMD    0x20
#define XNV_32K_BLOCK_ERASE_CMD 0x52
#define XNV_64K_BLOCK_ERASE_CMD 0xD8

#define XNV_STAT_BUSY  0x01
#define XNV_STAT_WIP   XNV_STAT_BUSY


/* ----------- XNV ---------- */
#define XNV_SPI_BEGIN()             st(P1_4 = 0;)
#define XNV_SPI_TX(x)               st(U1CSR &= ~0x02; U1DBUF = (x);)
#define XNV_SPI_RX()                U1DBUF
#define XNV_SPI_WAIT_RXRDY()        st(while (!(U1CSR & 0x02));)
#define XNV_SPI_END()               st(P1_4 = 1;)

/*
                    U1GCR    28
  (256 + U1BAUD) x 2      / 2   * 32MHz

  U1GCR.BAUD_E[4:0]
  U1BAUD.BAUD_M[7:0]
*/
// The TI reference design uses UART1 Alt. 2 in SPI mode.
#if 1
#define XNV_SPI_INIT_ELARA() \
st( \
  /* Mode select UART1 SPI Mode as master. */\
  U1CSR = 0; \
  \
  /* Setup for 115200 baud. */\
  U1GCR = 12; \
  U1BAUD = 216; \
  \
  /* Set bit order to MSB */\
  U1GCR |= BV(5); \
  \
  /* Set UART1 I/O to alternate 2 location on P1 pins. */\
  PERCFG |= 0x02;  /* U1CFG */\
  \
  /* Select peripheral function on I/O pins but SS is left as GPIO for separate control. */\
  P1SEL |= 0xE0;  /* SELP1_[7:4] */\
  /* P1.1,2,3: reset, LCD CS, XNV CS. */\
  P1SEL &= ~0x0E; \
  P1 |= 0x0E; \
  P1_1 = 0; \
  P1DIR |= 0x0E; \
  \
  /* Give UART1 priority over Timer3. */\
  P2SEL &= ~0x20;  /* PRI2P1 */\
  \
  /* When SPI config is complete, enable it. */\
  U1CSR |= 0x40; \
  /* Release XNV reset. */\
  P1_1 = 1; \
)
#else
#define XNV_SPI_INIT_ELARA() \
st( \
  /* Mode select UART1 SPI Mode as master. */\
  U1CSR = 0; \
  \
  /* Setup for 115200 baud. */\
  U1GCR = 15; \
  U1BAUD = 255; \
  \
  /* Set bit order to MSB */\
  U1GCR |= BV(5); \
  \
  /* Set UART1 I/O to alternate 2 location on P1 pins. */\
  PERCFG |= 0x02;  /* U1CFG */\
  \
  /* Select peripheral function on I/O pins but SS is left as GPIO for separate control. */\
  P1SEL |= 0xE0;  /* SELP1_[7:4] */\
  /* P1.1,2,3: reset, LCD CS, XNV CS. */\
  P1SEL &= ~0x0E; \
  P1 |= 0x0E; \
  P1_1 = 0; \
  P1DIR |= 0x0E; \
  \
  /* Give UART1 priority over Timer3. */\
  P2SEL &= ~0x20;  /* PRI2P1 */\
  \
  /* When SPI config is complete, enable it. */\
  U1CSR |= 0x40; \
  /* Release XNV reset. */\
  P1_1 = 1; \
)
#endif
/*********************************************************************
 * LOCAL VARIABLES
 */
static uint32 g_AddressBegin = 0;
static uint32 g_AddressActive = 0;

static uint32 g_bin_size = 0;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void xnvSPIWrite(uint8 ch)
{
    XNV_SPI_TX(ch);
    XNV_SPI_WAIT_RXRDY();
}

static void HalSPIRead(uint32 addr, uint8 *pBuf, uint16 len)
{

    uint8 shdw = P1DIR;
    P1DIR |= BV(3);

    XNV_SPI_BEGIN();
    do
    {
        xnvSPIWrite(XNV_STAT_CMD);
    } while (XNV_SPI_RX() & XNV_STAT_WIP);
    XNV_SPI_END();
    asm("NOP");
    asm("NOP");

    XNV_SPI_BEGIN();
    xnvSPIWrite(XNV_READ_CMD);
    xnvSPIWrite(addr >> 16);
    xnvSPIWrite(addr >> 8);
    xnvSPIWrite(addr);
    xnvSPIWrite(0);

    while (len--)
    {
        xnvSPIWrite(0);
        *pBuf++ = XNV_SPI_RX();
    }
    XNV_SPI_END();

    P1DIR = shdw;
}

static void HalSPIWrite(uint32 addr, uint8 *pBuf, uint16 len)
{
    uint8 cnt;
    uint8 shdw = P1DIR;
    P1DIR |= BV(3);
    while (len)
    {
        XNV_SPI_BEGIN();
        do
        {
            xnvSPIWrite(XNV_STAT_CMD);
        } while (XNV_SPI_RX() & XNV_STAT_WIP);
        XNV_SPI_END();
        asm("NOP");
        asm("NOP");

        XNV_SPI_BEGIN();
        xnvSPIWrite(XNV_WREN_CMD);
        XNV_SPI_END();
        asm("NOP");
        asm("NOP");

        XNV_SPI_BEGIN();
        xnvSPIWrite(XNV_WRPG_CMD);
        xnvSPIWrite(addr >> 16);
        xnvSPIWrite(addr >> 8);
        xnvSPIWrite(addr);

        // Can only write within any one page boundary, so prepare for next page write if bytes remain.
        cnt = 0 - (uint8)addr;
        if (cnt)
        {
            addr += cnt;
        }
        else
        {
            addr += 256;
        }

        do
        {
            xnvSPIWrite(*pBuf++);
            cnt--;
            len--;
        }
        while (len && cnt);
        XNV_SPI_END();
    }
    P1DIR = shdw;
}

int HalSPIEraseChip(void)
{
    //XNV_SPI_INIT_ELARA();

    uint8 shdw = P1DIR;
    P1DIR |= BV(3);
    XNV_SPI_BEGIN();
    do
    {
        xnvSPIWrite(XNV_STAT_CMD);
    }
    while (XNV_SPI_RX() & 0x01);
    XNV_SPI_END();
    asm("NOP");
    asm("NOP");

    XNV_SPI_BEGIN();
    xnvSPIWrite(XNV_WREN_CMD);
    XNV_SPI_END();
    asm("NOP");
    asm("NOP");

    XNV_SPI_BEGIN();
    //xnvSPIWrite(0x60);
    xnvSPIWrite(0xC7);
    XNV_SPI_END();

    P1DIR = shdw;

    return 2;
}

bool HalSPIEraseChipFinished(void)
{
    uint8 shdw = P1DIR;
    P1DIR |= BV(3);
    XNV_SPI_BEGIN();
    xnvSPIWrite(XNV_STAT_CMD);
    uint8 rxval = XNV_SPI_RX();
    XNV_SPI_END();

    P1DIR = shdw;
    if (rxval & 0x01)
    {
        return FALSE;
    }
    return TRUE;
}
/**************************************************************************************************
软件延时
这里要注意了，若使用uint32类型，时间将延长5倍！！！
如
void HalHW_WaitUs(uint32 microSecs)
 **************************************************************************************************/
void HalHW_WaitUs(uint16 microSecs)
{
    while(microSecs--)
    {
        /* 3 NOPs == 1 usecs */
        asm("NOP");
        asm("NOP");
        asm("NOP");

    }
}

void HalHW_WaitMS(uint16 ms)
{
    while(ms--)
    {
        int i = 0;
        for(i = 20; i > 0; i--)
            HalHW_WaitUs(50);
    }
}

static uint8 buf[8] = "1234";
//uint8 bufrx[10];
int test_spi_get_status_register2(uint8 *pBuf, uint8 len)
{
    XNV_SPI_INIT_ELARA();

    //HalSPIWrite(0x0, buf, 5);
    //注意，连续读写之间至少要延时800us
    //  HalHW_WaitUs(800);
    HalSPIRead(0x0, pBuf, 8);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////
int elara_flash_read_init_spi_w25q(void)
{
    XNV_SPI_INIT_ELARA();
    uint8 ret = osal_snv_read(BLE_NVID_MCU_BIN_SIZE, 4, &g_bin_size);
    if (ret != SUCCESS)
    {
        g_bin_size = 0;
    }
    if (g_bin_size == 0)
    {
        return ELARA_ERROR_105;
    }
    ret = osal_snv_read(BLE_NVID_ADR_BEGIN, 4, &g_AddressBegin);
    if (ret != SUCCESS)
    {
        g_AddressBegin = 0;
    }

    g_AddressActive = g_AddressBegin;
    return 0;
}

void elara_flash_write_init_spi_w25q(void)
{
    
    uint8 ret;
    ret = osal_snv_read(BLE_NVID_ADR_ACTIVE, 4, &g_AddressActive);
    if (ret != SUCCESS)
    {
        g_AddressActive = SPI_ADDRESS_BEGIN;
    }

    g_AddressBegin = g_AddressActive;
    g_bin_size = 0;
}

/*********************************************************************
 * @fn      elara_flash_if_flash_enough
 *
 * @brief   Before write bin to flash, we should check flash memory big enough.
            (Should run elara_flash_write_init function first.)
 *
 * @param   binSize - The size of the bin
 *
 * @return  The aviliabel flash size, 0 fail.
 */
uint32 elara_flash_if_flash_enough_spi_w25q(uint32 binSize)
{
    uint32 allFlashSize;

    // The aviliable flash size now.
    uint32 flashsizeAV = (uint32)SPI_FLASH_SIZE - g_AddressActive;

    // 4 bytes align
    uint32 real = ((binSize + (uint32)SPI_FLASH_WORD_SIZE - 1) / (uint32)SPI_FLASH_WORD_SIZE) * (uint32)SPI_FLASH_WORD_SIZE;//4

    if (SPI_FLASH_SIZE < g_AddressActive)
    {
        goto enoughExits;
    }
    if (real < flashsizeAV)
    {
        return flashsizeAV;
    }
enoughExits:
    P1_1 = 1; // Blue LED.
    P1_0 = 1; // Green LED.
    allFlashSize = SPI_FLASH_SIZE;
    if (real < allFlashSize)
    {
        return 0xFFFFFFFF;
    }
    return 0;
}

int elara_flash_erase_spi_w25q(void)
{
    XNV_SPI_INIT_ELARA();
    g_AddressActive = SPI_ADDRESS_BEGIN;
    g_AddressBegin = g_AddressActive;
    osal_snv_write(BLE_NVID_ADR_BEGIN, 4, &g_AddressBegin);
    osal_snv_write(BLE_NVID_ADR_ACTIVE, 4, &g_AddressActive);
    HalSPIEraseChip();
    return 1;
}

bool elara_flash_if_erase_finished_spi_w25q(void)
{
    uint8 shdw = P1DIR;
    P1DIR |= BV(3);
    XNV_SPI_BEGIN();
    xnvSPIWrite(XNV_STAT_CMD);
    uint8 rxval = XNV_SPI_RX();
    XNV_SPI_END();

    P1DIR = shdw;
    if (rxval & 0x01)
    {
        return FALSE;
    }
    return TRUE;
}

/*********************************************************************
 * @fn      elara_flash_read
 *
 * @brief   Read data from flash.
 *
 * @param   len - len%256 = 0
 * @param   pBuf - Data is read into this buffer.
 *
 * @return
 */
void elara_flash_read_spi_w25q( uint16 len, uint8 *pBuf )
{
    HalSPIRead(g_AddressActive, pBuf, len);
    g_AddressActive += len;
    //HalHW_WaitUs(800);
}

/*********************************************************************
 * @fn      elara_flash_write
 *
 * @brief   Writes 'len' bytes to the internal flash.
 *
 * @param   len - len%4 must be zero and len must < HAL_FLASH_PAGE_SIZE
 * @param   pBuf - Valid buffer space at least as big as 'len' X 4.
 *
 * @return
 */
int elara_flash_write_spi_w25q(uint16 len, uint8 *pBuf)
{
    static bool ifFirst = TRUE;
    if (ifFirst == TRUE)
    {
        XNV_SPI_INIT_ELARA();
        ifFirst = FALSE;
    }
    HalSPIWrite(g_AddressActive, pBuf, len);
    g_bin_size += len;
    g_AddressActive += len;
    //HalHW_WaitUs(800);
    return 0;
}

int elara_flash_write_done_spi_w25q(void)
{
    //P0_1 = 1; // Orange LED.
    uint8 ret;

    // SPI_PAGE_SIZE bytes align.
    uint32 real = ((g_AddressActive + (uint32)SPI_PAGE_SIZE - 1) / (uint32)SPI_PAGE_SIZE) * (uint32)SPI_PAGE_SIZE; //4
    osal_snv_write(BLE_NVID_MCU_BIN_SIZE, 4, &g_bin_size);
    osal_snv_write(BLE_NVID_ADR_BEGIN, 4, &g_AddressBegin);
    osal_snv_write(BLE_NVID_ADR_ACTIVE, 4, &real);

    uint8 uState = 1;
    ret = osal_snv_write(BLE_NVID_U_STATE, 1, &uState);

    return ret;
}

// When lost connection or upgrade fail, we should save current active page and offset.
void elara_flash_lost_connected_spi_w25q(void)
{
    // SPI_PAGE_SIZE bytes align.
    uint32 real = ((g_AddressActive + (uint32)SPI_PAGE_SIZE - 1) / (uint32)SPI_PAGE_SIZE) * (uint32)SPI_PAGE_SIZE; //4
    osal_snv_write(BLE_NVID_ADR_ACTIVE, 4, &real);
}

uint32 elara_flash_any_thing_else_spi_w25q(void)
{
    uint8 ret;
    ret = osal_snv_read(BLE_NVID_ADR_ACTIVE, 4, &g_AddressActive);
    if (ret != SUCCESS)
    {
        g_AddressActive = SPI_ADDRESS_BEGIN;
    }

    g_AddressBegin = g_AddressActive;
    g_bin_size = 0;
    return 0;
}