/**************************************************************************************************
  Filename:       md_profile.h
  Revised:        $Date: 2015-09-10 15:41:26 +0800 (Thu, 19 Sep 2015) $
  Revision:       $Revision: 0 $

**************************************************************************************************/
#ifndef MD_UART_PROFILE_H
#define MD_UART_PROFILE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"

/*********************************************************************
 * CONSTANTS
 */

#define MD_UART_SERVICE_UUID      0xFFF0
#define MD_UART_BIN_SIZE_UUID     0xFFF1
#define MD_UART_BIN_BLOCK_UUID    0xFFF2
#define MD_UART_ERROR_RESET_UUID  0xFFF3
#define MD_UART_GET_PART_ID_UUID  0xFFF4
#define MD_UART_TRANS_UUID        0xFFF6

// OAD Characteristic Indices
#define MD_UART_CHAR_IMG_IDENTIFY 0
#define MD_UART_CHAR_IMG_BLOCK    1

#define MD_UART_TRANS_LEN          19

// Profile Parameters
#define MD_UART_PROFILE_TRANS      5

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * Profile Callbacks
 */

// Callback when a characteristic value has changed
typedef void (*mdUartProfileChange_t)( uint8 paramID );
typedef struct
{
  mdUartProfileChange_t        pfnMDProfileChange;  // Called when characteristic value changes
} mdUartProfileCBs_t;

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * @fn      MDProfile_AddService
 *
 * @brief   Initializes the Microduino Service by registering GATT attributes
 *          with the GATT server. Only call this function once.
 *
 * @return  Success or Failure
 */
bStatus_t MDUARTProfile_AddService( void );
bStatus_t MDUARTProfile_GetParameter( uint8 param, void *value, uint8 *returnBytes);
bStatus_t MDUARTProfile_SetParameter( uint8 param, uint8 len, void *value );
//bStatus_t MDProfile_SetParameter( uint8 param, uint8 len, void *value );
extern bStatus_t MDUARTProfile_RegisterAppCBs( mdUartProfileCBs_t *appCallbacks );
//extern void MDBeforeReset(void);
extern bool MDUARTSerialAppSendNoti(uint8 *pBuffer,uint16 length);
extern bool MDUARTIfNotifyOpened(void);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* OAD_TARGET_H */
