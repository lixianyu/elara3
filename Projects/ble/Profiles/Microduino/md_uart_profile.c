/**************************************************************************************************
  Filename:       md_profile.c
  Revised:        $Date: 2015-09-10 15:49:55 +0800 (Thu, 10 Sep 2015) $
  Revision:       $Revision: 0 $

**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include "bcomdef.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "hal_aes.h"
#include "hal_crc.h"
#include "hal_flash.h"
#include "hal_dma.h"
#include "hal_types.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"
#include "peripheral.h"
#include "elara_file_common.h"
#include "md_uart_profile.h"
#include "osal_snv.h"
//#define DEBUG_SERIAL
#include "SerialApp.h"


extern void startNotifyCheck(void);
/*********************************************************************
 * CONSTANTS
 */
static mdUartProfileCBs_t *mdProfile_AppCBs = NULL;
/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

// MD Service UUID
static CONST uint8 mdServUUID[ATT_BT_UUID_SIZE] =
{
    LO_UINT16(MD_UART_SERVICE_UUID), HI_UINT16(MD_UART_SERVICE_UUID),
    //TI_BASE_UUID_128( MD_SERVICE_UUID )
};

static CONST uint8 mdCharUUID[1][ATT_BT_UUID_SIZE] =
{
    LO_UINT16(MD_UART_TRANS_UUID), HI_UINT16(MD_UART_TRANS_UUID),
};

/*********************************************************************
 * Profile Attributes - variables
 */

// MD Service attribute
static CONST gattAttrType_t mdService = { ATT_BT_UUID_SIZE, mdServUUID };

// Place holders for the GATT Server App to be able to lookup handles.
static uint8 mdTRANSChar[MD_UART_TRANS_LEN] = {0};
//static uint8 mdPardID[2] = {0};
// MD Characteristic Properties
static uint8 mdCharProps = GATT_PROP_WRITE_NO_RSP | GATT_PROP_READ | GATT_PROP_NOTIFY;
static uint8 mdTRANSLen = 0;
// MD Client Characteristic Configs
static gattCharCfg_t *mdTransConfig;

// MD Characteristic user descriptions
static CONST uint8 mdTransDesc[] = "Trans";
/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t mdAttrTbl[] =
{
    // MD Service
    {
        { ATT_BT_UUID_SIZE, primaryServiceUUID },
        GATT_PERMIT_READ,
        0,
        (uint8 *) &mdService
    },

//..................................................................
    // MD TRANS Characteristic Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &mdCharProps
    },

    // MD TRANS Characteristic Value
    {
        { ATT_BT_UUID_SIZE, mdCharUUID[0] },
        GATT_PERMIT_WRITE,
        0,
        mdTRANSChar
    },

    // Characteristic configuration
    {
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8 *) &mdTransConfig
    },

    // MD TRANS User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        (uint8 *)mdTransDesc
    },
};

/*********************************************************************
 * LOCAL VARIABLES
 */

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static bStatus_t mdReadAttrCB(uint16 connHandle, gattAttribute_t *pAttr,
                              uint8 *pValue, uint8 *pLen, uint16 offset,
                              uint8 maxLen, uint8 method);

static bStatus_t mdWriteAttrCB(uint16 connHandle, gattAttribute_t *pAttr,
                               uint8 *pValue, uint8 len, uint16 offset,
                               uint8 method);

static CONST gattServiceCBs_t mdCBs =
{
    mdReadAttrCB,  // Read callback function pointer.
    mdWriteAttrCB, // Write callback function pointer.
    NULL            // Authorization callback function pointer.
};

/*********************************************************************
 * @fn      MDProfile_AddService
 *
 * @brief   Initializes the Microduino Service by registering GATT attributes
 *          with the GATT server. Only call this function once.
 *
 * @return  The return value of GATTServApp_RegisterForMsg().
 */
bStatus_t MDUARTProfile_AddService(void)
{
    // Allocate Client Characteristic Configuration table
    mdTransConfig = (gattCharCfg_t *)osal_mem_alloc( sizeof(gattCharCfg_t) *
                       linkDBNumConns);
    if (mdTransConfig == NULL)
    {
        return ( bleMemAllocError );
    }
    // Initialize Client Characteristic Configuration attributes
    GATTServApp_InitCharCfg( INVALID_CONNHANDLE, mdTransConfig );

    return GATTServApp_RegisterService(mdAttrTbl, GATT_NUM_ATTRS(mdAttrTbl),
                                       GATT_MAX_ENCRYPT_KEY_SIZE, &mdCBs);
}

/*********************************************************************
 * @fn      mdReadAttrCB
 *
 * @brief   Read an attribute.
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be read
 * @param   pLen - length of data to be read
 * @param   offset - offset of the first octet to be read
 * @param   maxLen - maximum length of data to be read
 * @param   method - type of read message
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t mdReadAttrCB(uint16 connHandle, gattAttribute_t *pAttr,
                              uint8 *pValue, uint8 *pLen, uint16 offset,
                              uint8 maxLen, uint8 method)
{
    bStatus_t status = SUCCESS;

    // Make sure it's not a blob operation (no attributes in the profile are long)
    if ( offset > 0 )
    {
        //return ( ATT_ERR_ATTR_NOT_LONG );
    }
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch (uuid)
    {
        case MD_UART_TRANS_UUID:
        *pLen = mdTRANSLen;
        VOID osal_memcpy( pValue, pAttr->pValue, mdTRANSLen );
        {
          // 这个变量用于表明上一次写数据到从机已经成功， 可用于判断写数据时的判断， 以确保数据的完整性
          extern bool simpleBLEChar6DoWrite2;
          simpleBLEChar6DoWrite2 = TRUE;
        }
        break;
        default:
          *pLen = 0;
          status = ATT_ERR_ATTR_NOT_FOUND;
    }

    return status;
}

/*********************************************************************
 * @fn      mdWriteAttrCB
 *
 * @brief   Validate and Write attribute data
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message
 *
 * @return  SUCCESS, blePending or Failure
 */
static uint16 gConnHandle = 0;
static bStatus_t mdWriteAttrCB(uint16 connHandle, gattAttribute_t *pAttr,
                               uint8 *pValue, uint8 len, uint16 offset,
                               uint8 method)
{
    bStatus_t status = SUCCESS;
    
    if ( pAttr->type.len == ATT_BT_UUID_SIZE )
    {
        // 16-bit UUID
        uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
        switch ( uuid )
        {
            case MD_UART_TRANS_UUID:
                if ( offset == 0 )
                {
                    if ( len > MD_UART_TRANS_LEN)
                    {
                        status = ATT_ERR_INVALID_VALUE_SIZE;
                    }
                }
                else
                {
                    status = ATT_ERR_ATTR_NOT_LONG;
                }
                if ( status == SUCCESS )
                {
                    #if 0
                    uint8 *pCurValue = (uint8 *)pAttr->pValue;
		            memset(pCurValue, 0, MD_UART_TRANS_LEN);
                    memcpy(pCurValue, pValue, len );
                    #else
                    VOID osal_memcpy( pAttr->pValue, pValue, len );
                    mdTRANSLen = len;
                    #endif
                    mdProfile_AppCBs->pfnMDProfileChange( MD_UART_PROFILE_TRANS );
                }
                break;
                
            case GATT_CLIENT_CHAR_CFG_UUID:
                status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                         offset, GATT_CLIENT_CFG_NOTIFY );
                startNotifyCheck();
                break;
            default:
                // Should never get here!
                status = ATT_ERR_ATTR_NOT_FOUND;
                break;
        }
    }
    else
    {
    }

    return status;
}

bool MDUARTIfNotifyOpened(void)
{
    uint16 value = GATTServApp_ReadCharCfg( gConnHandle, mdTransConfig );

    // If notifications enabled
    if ( value & GATT_CLIENT_CFG_NOTIFY )
    {
        return TRUE;
    }
    return FALSE;
}

bool MDUARTSerialAppSendNoti(uint8 *pBuffer, uint16 length)
{
    GAPRole_GetParameter(GAPROLE_CONNHANDLE, &gConnHandle);
    if (gConnHandle == INVALID_CONNHANDLE || length == 0)
    {
        return FALSE;
    }
    uint16 value = GATTServApp_ReadCharCfg( gConnHandle, mdTransConfig );

    // If notifications enabled
    if ( value & GATT_CLIENT_CFG_NOTIFY )
    {
        gattAttribute_t *pAttr = GATTServApp_FindAttr(mdAttrTbl, GATT_NUM_ATTRS(mdAttrTbl),
                                 mdTRANSChar);
        if ( pAttr != NULL )
        {
            attHandleValueNoti_t noti;

            noti.pValue = GATT_bm_alloc(gConnHandle, ATT_HANDLE_VALUE_NOTI,
                                        length, NULL);
            if ( noti.pValue != NULL )
            {
                noti.handle = pAttr->handle;
                noti.len = length;
                memcpy(noti.pValue, pBuffer, length);
                if ( GATT_Notification(gConnHandle, &noti, FALSE) != SUCCESS )
                {
                    GATT_bm_free((gattMsg_t *)&noti, ATT_HANDLE_VALUE_NOTI);
                }
            }
        }
    }
    return TRUE;
}

bStatus_t MDUARTProfile_RegisterAppCBs( mdUartProfileCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
    mdProfile_AppCBs = appCallbacks;
    
    return ( SUCCESS );
  }
  else
  {
    return ( bleAlreadyInRequestedMode );
  }
}

bStatus_t MDUARTProfile_GetParameter( uint8 param, void *value, uint8 *returnBytes)
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case MD_UART_PROFILE_TRANS:
        memcpy( value, mdTRANSChar, mdTRANSLen);
        *returnBytes = mdTRANSLen;
        break;
        
    default:
      ret = INVALIDPARAMETER;
      break;
  }
  
  return ( ret );
}

bStatus_t MDUARTProfile_SetParameter( uint8 param, uint8 len, void *value )
{
    bStatus_t ret = SUCCESS;
    switch ( param )
    {
    case MD_UART_PROFILE_TRANS:
      if ( len <= MD_UART_TRANS_LEN ) 
      {
        VOID osal_memcpy( mdTRANSChar, value, len );
        mdTRANSLen = len;
        
        // See if Notification has been enabled
        GATTServApp_ProcessCharCfg( mdTransConfig, mdTRANSChar, FALSE,
                                    mdAttrTbl, GATT_NUM_ATTRS( mdAttrTbl ),
                                    INVALID_TASK_ID, mdReadAttrCB );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
      
    default:
      ret = INVALIDPARAMETER;
      break;
    }
    return ret;
}
/*********************************************************************
*********************************************************************/
