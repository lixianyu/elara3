/**************************************************************************************************
  Filename:       md_profile.h
  Revised:        $Date: 2015-09-10 15:41:26 +0800 (Thu, 19 Sep 2015) $
  Revision:       $Revision: 0 $

**************************************************************************************************/
#ifndef MD_PROFILE_H
#define MD_PROFILE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"

/*********************************************************************
 * CONSTANTS
 */

#define MD_SERVICE_UUID      0xFFE0
#define MD_BIN_SIZE_UUID     0xFFE1
#define MD_BIN_BLOCK_UUID    0xFFE2
#define MD_ERROR_RESET_UUID  0xFFE3
#define MD_GET_PART_ID_UUID  0xFFE4
//#define MD_TRANS_UUID        0xFFE6

// OAD Characteristic Indices
#define MD_CHAR_IMG_IDENTIFY 0
#define MD_CHAR_IMG_BLOCK    1

#define MD_BLOCK_SIZE         16
#define MD_BLOCK_SIZE_BIG     20


#define MD_TRANS_LEN                    40

// Profile Parameters
#define MDPROFILE_ERASE                 1
#define MDPROFILE_UPDATE_AVR            2
#define MDPROFILE_RESET_MANUAL          3
#define MDPROFILE_PARTID                4
#define MDPROFILE_TRANS                 5
#define MDPROFILE_STOP_ME               6
#define MDPROFILE_OPEN_UART             7
#define MDPROFILE_CLOSE_UART            8
/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * Profile Callbacks
 */

// Callback when a characteristic value has changed
typedef void (*mdProfileChange_t)( uint8 paramID );
typedef struct
{
  mdProfileChange_t        pfnMDProfileChange;  // Called when characteristic value changes
} mdProfileCBs_t;

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * @fn      MDProfile_AddService
 *
 * @brief   Initializes the Microduino Service by registering GATT attributes
 *          with the GATT server. Only call this function once.
 *
 * @return  Success or Failure
 */
bStatus_t MDProfile_AddService( void );
bStatus_t MDProfile_GetParameter( uint8 param, void *value );
bStatus_t MDProfile_SetParameter( uint8 param, uint8 len, void *value );
extern bStatus_t MDProfile_RegisterAppCBs( mdProfileCBs_t *appCallbacks );
extern void MDBeforeReset(void);
extern bool MDSerialAppSendNoti(uint8 *pBuffer,uint16 length);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* OAD_TARGET_H */
